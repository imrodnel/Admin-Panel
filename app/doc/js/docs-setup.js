NG_DOCS={
  "sections": {
    "api": "Documentation de l'interface d'administration"
  },
  "pages": [
    {
      "section": "api",
      "id": "Application d'administration de la compétition",
      "shortName": "Application d'administration de la compétition",
      "type": "overview",
      "moduleName": "Application d'administration de la compétition",
      "shortDescription": "Module principal de l&#39;application. Se charge de gérer les vues et controleurs a utiliser en fonction de la route",
      "keywords": "administration api application charge comp controleurs de en fonction la les module overview principal route se utiliser vues"
    },
    {
      "section": "api",
      "id": "panelAdminApp.apicall",
      "shortName": "panelAdminApp.apicall",
      "type": "service",
      "moduleName": "panelAdminApp",
      "shortDescription": "Ce service permet d&#39;effectuer tous les appel à l&#39;API",
      "keywords": "aaaa-mm-dd addjoueur ajouter ancien annuler api apicall appel associ au aupr authentification avoir cancelmatch ce changeendmatch changematchcourt changer code comme compatible concern concernant court courts dans datareq datefin de delequipe deljoueur des disponibles doc documentation doit du effectuer en endmatch entre etape file fin format gagnant genre getcourtsdispo getequipe getequipes getjoueurs getjoueursequipe getmatch getmatchs getscore getstage getsupermatchs gettirage gettournoi hh idancienjoueur identifiant idequipe idjoueur idmatch idnouveaujoueur indequipe informations infosauth infosmatch infosphoto infostirage initialisation iso isoequipe joueur joueurs jour la le lequel les match matchchangerjoueur matchs method mettre modifier nextsteptournoi nouveau nouveaucourt nouvelle nouvelles objet obtenir paneladminapp passer pays permet photo pour promise qu relative rencontres requ retrouver score se selon service setequipe setjoueur settirage sexe son souhaite sp startmatch suivant supermatchs supprimer sur terminer tirage token tour tournoi tournois tous type une update updatephoto upload utilisateur valeurs"
    },
    {
      "section": "api",
      "id": "panelAdminApp.ApiCaller",
      "shortName": "panelAdminApp.ApiCaller",
      "type": "object",
      "moduleName": "panelAdminApp",
      "shortDescription": "Objet retourné par le provider apicall",
      "keywords": "acc api apicall apicaller appel au aux correspondant de https image imags le method object objet paneladminapp par permet pour provider retourn serveur seturlapi seturlimages sp url"
    },
    {
      "section": "api",
      "id": "panelAdminApp.chargementGlobal",
      "shortName": "panelAdminApp.chargementGlobal",
      "type": "service",
      "moduleName": "panelAdminApp",
      "shortDescription": "Valeur AngularJS permettant de spécifier si un chargement est actif (true) ou non (false)",
      "keywords": "actif angularjs api chargement chargementglobal de est ou paneladminapp permettant service si sp valeur"
    },
    {
      "section": "api",
      "id": "panelAdminApp.controller:AjoutJoueurCtrl",
      "shortName": "AjoutJoueurCtrl",
      "type": "controller",
      "moduleName": "panelAdminApp",
      "shortDescription": "Contrôleur de fenêtre modal servant à saisir les informations nécessaires à l&#39;ajout d&#39;un joueur",
      "keywords": "ajout api contr controller de fen informations joueur les modal paneladminapp saisir servant"
    },
    {
      "section": "api",
      "id": "panelAdminApp.controller:AjoutJoueurEquipeCtrl",
      "shortName": "AjoutJoueurEquipeCtrl",
      "type": "controller",
      "moduleName": "panelAdminApp",
      "shortDescription": "Contrôleur de fenêtre modal servant à saisir les informations nécessaires à l&#39;ajout d&#39;un joueur dans une équipe",
      "keywords": "ajout api contr controller dans de fen informations joueur les modal paneladminapp saisir servant une"
    },
    {
      "section": "api",
      "id": "panelAdminApp.controller:AuthentificationCtrl",
      "shortName": "AuthentificationCtrl",
      "type": "controller",
      "moduleName": "panelAdminApp",
      "shortDescription": "Contrôleur de la page d&#39;authentification",
      "keywords": "api authentification contr controller de la paneladminapp"
    },
    {
      "section": "api",
      "id": "panelAdminApp.controller:EditerEquipeCtrl",
      "shortName": "EditerEquipeCtrl",
      "type": "controller",
      "moduleName": "panelAdminApp",
      "shortDescription": "Contrôleur de fenêtre modal servant à éditer un joueur",
      "keywords": "api contr controller de fen joueur modal paneladminapp servant"
    },
    {
      "section": "api",
      "id": "panelAdminApp.controller:EditerEquipeCtrl",
      "shortName": "EditerEquipeCtrl",
      "type": "controller",
      "moduleName": "panelAdminApp",
      "shortDescription": "Contrôleur de fenêtre modal servant à éditer une équipe",
      "keywords": "api contr controller de fen modal paneladminapp servant une"
    },
    {
      "section": "api",
      "id": "panelAdminApp.controller:EquipesCtrl",
      "shortName": "EquipesCtrl",
      "type": "controller",
      "moduleName": "panelAdminApp",
      "shortDescription": "Contrôleur de la page gérant les équipes",
      "keywords": "api contr controller de la les paneladminapp"
    },
    {
      "section": "api",
      "id": "panelAdminApp.controller:HeaderCtrl",
      "shortName": "HeaderCtrl",
      "type": "controller",
      "moduleName": "panelAdminApp",
      "shortDescription": "Contrôleur du header de l&#39;application (utilisé pour actualiser l&#39;affichage de l&#39;onglet actif)",
      "keywords": "actif actualiser affichage api application contr controller de du header onglet paneladminapp pour"
    },
    {
      "section": "api",
      "id": "panelAdminApp.controller:JoueursCtrl",
      "shortName": "JoueursCtrl",
      "type": "controller",
      "moduleName": "panelAdminApp",
      "shortDescription": "Controleur de la page de gestion des joueurs",
      "keywords": "api controleur controller de des gestion joueurs la paneladminapp"
    },
    {
      "section": "api",
      "id": "panelAdminApp.controller:JoueursEquipeCtrl",
      "shortName": "JoueursEquipeCtrl",
      "type": "controller",
      "moduleName": "panelAdminApp",
      "shortDescription": "Contrôleur de la page gérant les joueurs d&#39;une équipe",
      "keywords": "api contr controller de joueurs la les paneladminapp une"
    },
    {
      "section": "api",
      "id": "panelAdminApp.controller:MainCtrl",
      "shortName": "MainCtrl",
      "type": "controller",
      "moduleName": "panelAdminApp",
      "shortDescription": "Contrôleur de la page d&#39;accueil",
      "keywords": "accueil api contr controller de la paneladminapp"
    },
    {
      "section": "api",
      "id": "panelAdminApp.controller:MatchsCtrl",
      "shortName": "MatchsCtrl",
      "type": "controller",
      "moduleName": "panelAdminApp",
      "shortDescription": "Controleur de la page de gestion des matchs",
      "keywords": "api controleur controller de des gestion la matchs paneladminapp"
    },
    {
      "section": "api",
      "id": "panelAdminApp.controller:ModalInstanceCtrl",
      "shortName": "ModalInstanceCtrl",
      "type": "controller",
      "moduleName": "panelAdminApp",
      "shortDescription": "Contrôleur de fenêtre modal servant à lancer une fenêtre modal sans interaction particulière",
      "keywords": "api contr controller de fen interaction lancer modal paneladminapp particuli sans servant une"
    },
    {
      "section": "api",
      "id": "panelAdminApp.controller:ModalSupprimerjoueurCtrl",
      "shortName": "ModalSupprimerjoueurCtrl",
      "type": "controller",
      "moduleName": "panelAdminApp",
      "shortDescription": "ModalSupprimerjoueurCtrl",
      "keywords": "administrationacecreamapp api controller modalsupprimerjoueurctrl paneladminapp"
    },
    {
      "section": "api",
      "id": "panelAdminApp.controller:ShowPhotoCtrl",
      "shortName": "ShowPhotoCtrl",
      "type": "controller",
      "moduleName": "panelAdminApp",
      "shortDescription": "Permet d&#39;afficher la photo d&#39;un joueur dans un modal",
      "keywords": "afficher api controller dans joueur la modal paneladminapp permet photo"
    },
    {
      "section": "api",
      "id": "panelAdminApp.controller:TerminerMatchCtrl",
      "shortName": "TerminerMatchCtrl",
      "type": "controller",
      "moduleName": "panelAdminApp",
      "shortDescription": "Modal servant à saisir les informations nécessaires pour terminer un match",
      "keywords": "api controller informations les match modal paneladminapp pour saisir servant terminer"
    },
    {
      "section": "api",
      "id": "panelAdminApp.controller:TirageCtrl",
      "shortName": "TirageCtrl",
      "type": "controller",
      "moduleName": "panelAdminApp",
      "shortDescription": "Contrôleur de la page de gérant l&#39;initialisation des rencontres via tirage",
      "keywords": "api contr controller de des initialisation la paneladminapp rencontres tirage"
    },
    {
      "section": "api",
      "id": "panelAdminApp.directive:afficheurInfos",
      "shortName": "afficheurInfos",
      "type": "directive",
      "moduleName": "panelAdminApp",
      "shortDescription": "Cette directive agit en collaboration avec le service informer et est chargée de générer le code HTML d&#39;affichage des notifications au sein de l&#39;application",
      "keywords": "affichage agit api application au avec cette charg code collaboration de des directive en est html informer le notifications paneladminapp sein service"
    },
    {
      "section": "api",
      "id": "panelAdminApp.directive:chargement",
      "shortName": "chargement",
      "type": "directive",
      "moduleName": "panelAdminApp",
      "shortDescription": "Cette directive est chargée d&#39;afficher une animation de chargement",
      "keywords": "afficher animation api bool cette charg chargement charger container contenu de directive doit en englob est juste le local ou paneladminapp parent prendre si sp statuschargement tout une"
    },
    {
      "section": "api",
      "id": "panelAdminApp.directive:fileModel",
      "shortName": "fileModel",
      "type": "directive",
      "moduleName": "panelAdminApp",
      "shortDescription": "Cette directive est charger de surveiller un input de type file car celui-ci n&#39;est pas pris en charge par AngularJS",
      "keywords": "angularjs api car celui-ci cette charge charger de directive en est file input paneladminapp par pas pris surveiller type"
    },
    {
      "section": "api",
      "id": "panelAdminApp.directive:flagSelector",
      "shortName": "flagSelector",
      "type": "directive",
      "moduleName": "panelAdminApp",
      "shortDescription": "Cette directive est chargée d&#39;afficher une liste de pays avec drapeaux associés et la possibilité d&#39;en sélectionner un",
      "keywords": "actuellement afficher api associ avec cette charg de directive drapeaux en est la le liste paneladminapp pays possibilit selection une"
    },
    {
      "section": "api",
      "id": "panelAdminApp.directive:focusOn",
      "shortName": "focusOn",
      "type": "directive",
      "moduleName": "panelAdminApp",
      "shortDescription": "Cette directive est chargée de déclencher le focus de l&#39;élément sur lequel elle est située lorsque elemFocus change",
      "keywords": "afin api cette change changement chaque charg de directive element elemfocus elle est focus le lequel lorsque paneladminapp prendre situ sur surveiller"
    },
    {
      "section": "api",
      "id": "panelAdminApp.directive:listSelector",
      "shortName": "listSelector",
      "type": "directive",
      "moduleName": "panelAdminApp",
      "shortDescription": "Cette directive est chargée de générer un bouton imitant le type select ",
      "keywords": "affichage api appliquer bouton cette champs champsnom charg choisi choix classes css de des design directive du entr entrees est imitant le les objet objets ou paneladminapp par plusieures pour select type une utilis utilisateur"
    },
    {
      "section": "api",
      "id": "panelAdminApp.directive:ongletMatch",
      "shortName": "ongletMatch",
      "type": "directive",
      "moduleName": "panelAdminApp",
      "shortDescription": "Utilisant le système d&#39;ascenceur pour l&#39;affichage des matchs, ongletMatch fournit une marche de cet ascenceur (un onglet dépliable) permettant de modifier le status et les informations d&#39;un match",
      "keywords": "affichage api ascenceur auquel cet de des directive doit faire fournit informations le les marche match matchs modifier onglet ongletmatch paneladminapp permettant pour status syst une utilisant"
    },
    {
      "section": "api",
      "id": "panelAdminApp.directive:tableau",
      "shortName": "tableau",
      "type": "directive",
      "moduleName": "panelAdminApp",
      "shortDescription": "Directive définissant un tableau éditable à plusieurs entrées",
      "keywords": "activer ajouter api associ associer attribut au bontons bootstrap bouton boutons ce cet chaque charg classe clic clique colonnes dans de des dire directive doit du editable edition entr est exc exceder filtrable filtrables filtrage filtre fonction forme glyph glyphicon il include informations infosboutons infoscolonnes la le les lors lorsqu manager ne nom objet objets options optionscolonne optionstab paginable paneladminapp pas plusieurs pour qui recense repr sens si sinon sp style suivante sur tableau taille template total triable triage type une utiliser"
    },
    {
      "section": "api",
      "id": "panelAdminApp.EquipesManager",
      "shortName": "panelAdminApp.EquipesManager",
      "type": "service",
      "moduleName": "panelAdminApp",
      "shortDescription": "Ce manager est chargé de gérer les entrées des équipes retournées par l&#39;API et gérer les messages d&#39;erreur relatifs à son appel",
      "keywords": "api appel ce celle charg de des du editerentree entr equipe equipesmanager erreur est identique les manager messages method modifier paneladminapp par permet rafraichir relatifs retourn service son supprimerentree une"
    },
    {
      "section": "api",
      "id": "panelAdminApp.filter:applatirScore",
      "shortName": "applatirScore",
      "type": "filter",
      "moduleName": "panelAdminApp",
      "shortDescription": "Filtre qui permet de préparer un objet contenant le score d&#39;un match pour appel à l&#39;API",
      "keywords": "api appel contenant de filter filtre le les match objet paneladminapp permet pour pr qui score scores source"
    },
    {
      "section": "api",
      "id": "panelAdminApp.filter:applatirTirage",
      "shortName": "applatirTirage",
      "type": "filter",
      "moduleName": "panelAdminApp",
      "shortDescription": "Ce filtre est chargé de transformer le tableau d&#39;objets reçu via l&#39;API en un tableau simple",
      "keywords": "api ce charg de en est filter filtre function le les objets paneladminapp simple source tableau tirages transformer"
    },
    {
      "section": "api",
      "id": "panelAdminApp.filter:assocFilter",
      "shortName": "assocFilter",
      "type": "filter",
      "moduleName": "panelAdminApp",
      "shortDescription": "Ce filtre est chargé de transformer les clés d&#39;un objet.",
      "keywords": "anciennecle api assoc associations bien ce champs charg cl complet conserver dans de doit est filter filtre forme function ignorer indique la le les modifier ne nouvellecle objet origine ou paneladminapp pas qui renseign renseignant si sont source sous transformer"
    },
    {
      "section": "api",
      "id": "panelAdminApp.filter:extension",
      "shortName": "extension",
      "type": "filter",
      "moduleName": "panelAdminApp",
      "shortDescription": "Permet d&#39;extraire l&#39;extension depuis un nom de fichier",
      "keywords": "api de depuis extension extraire fichier filter nom nomfichier paneladminapp permet"
    },
    {
      "section": "api",
      "id": "panelAdminApp.filter:prepareTirage",
      "shortName": "prepareTirage",
      "type": "filter",
      "moduleName": "panelAdminApp",
      "shortDescription": "Ce filtre est chargé de transformer le tableau à envoyer à l&#39;API de façon à ce qu&#39;il correponde à l&#39;entrée attendue",
      "keywords": "api attendue ce charg correponde de entr envoyer est fa filter filtre function il le paneladminapp qu source tableau transformer"
    },
    {
      "section": "api",
      "id": "panelAdminApp.filter:slice",
      "shortName": "slice",
      "type": "filter",
      "moduleName": "panelAdminApp",
      "shortDescription": "Ce filtre est chargé de retourner une copie du tableau qui lui est passé en paramètre en ne conservant que les éléments ayant un indice situé dans la plage définie par les autres paramètres.",
      "keywords": "api autres ayant ce charg conservant copie dans de debut du en entree est filter filtre fin indice la le les lui ne origine paneladminapp par param pass plage qui retourner situ tableau une"
    },
    {
      "section": "api",
      "id": "panelAdminApp.informer",
      "shortName": "panelAdminApp.informer",
      "type": "service",
      "moduleName": "panelAdminApp",
      "shortDescription": "informer",
      "keywords": "api factory informer paneladminapp service"
    },
    {
      "section": "api",
      "id": "panelAdminApp.informer",
      "shortName": "panelAdminApp.informer",
      "type": "object",
      "moduleName": "panelAdminApp",
      "shortDescription": "Objet gérant les notifications provenant de l&#39;application",
      "keywords": "ajouter api application au bleu centre danger de des du ensemble error getinformations info informations informer le les message messages method msg notifications object objet orange paneladminapp permet primary provenant retourne rouge success supprimer type vert warning"
    },
    {
      "section": "api",
      "id": "panelAdminApp.infosConnexion",
      "shortName": "panelAdminApp.infosConnexion",
      "type": "service",
      "moduleName": "panelAdminApp",
      "shortDescription": "Valeur AngularJS contenant les informations de connexion d&#39;un utilisateur (login, password, token)",
      "keywords": "angularjs api connexion contenant de informations infosconnexion les paneladminapp password service token utilisateur valeur"
    },
    {
      "section": "api",
      "id": "panelAdminApp.intercepteurHttp",
      "shortName": "panelAdminApp.intercepteurHttp",
      "type": "service",
      "moduleName": "panelAdminApp",
      "shortDescription": "L&#39;intercepteur HTTP est chargé de rediriger l&#39;utilisateur vers l&#39;authentification s&#39;il ne s&#39;est pas connecté avant d&#39;envoyer une requête de type &#39;édition&#39; à l&#39;API. Il permet également d&#39;insérer le token d&#39;authentification dans toutes les requêtes vers l&#39;API, de gérer les messages d&#39;erreur provenant de celle-ci et de gérer la reconnexion automatique si nécessaire.",
      "keywords": "api authentification automatique avant celle-ci charg connect dans de envoyer erreur est http il ins intercepteur intercepteurhttp la le les messages ne paneladminapp pas permet provenant reconnexion rediriger requ service si token toutes type une utilisateur vers"
    },
    {
      "section": "api",
      "id": "panelAdminApp.JoueursEquipeManager",
      "shortName": "panelAdminApp.JoueursEquipeManager",
      "type": "service",
      "moduleName": "panelAdminApp",
      "shortDescription": "Ce manager est chargé de gérer les entrées des joueurs d&#39;une équipe retournées par l&#39;API et gérer les messages d&#39;erreur relatifs à son appel",
      "keywords": "ajouter ajouterentree api appel au ce celui charg de des du editerentree entr erreur est faut il joueur joueurs joueursequipemanager le les manager messages method nouveau paneladminapp par permet qu rafraichir relatifs retourn service son supprimer supprimerentree une"
    },
    {
      "section": "api",
      "id": "panelAdminApp.JoueursManager",
      "shortName": "panelAdminApp.JoueursManager",
      "type": "service",
      "moduleName": "panelAdminApp",
      "shortDescription": "Ce manager est chargé de gérer les entrées des joueurs retournées par l&#39;API et gérer les messages d&#39;erreur relatifs à son appel",
      "keywords": "ajouter ajouterentree api appel au ce celui charg de des du editerentree entr erreur est faut il joueur joueurs joueursmanager le les manager messages method nouveau paneladminapp par permet qu rafraichir relatifs retourn service son supprimer supprimerentree"
    },
    {
      "section": "api",
      "id": "panelAdminApp.pays",
      "shortName": "panelAdminApp.pays",
      "type": "service",
      "moduleName": "panelAdminApp",
      "shortDescription": "Constante AngularJS chargée d&#39;associer un nom de pays à son code ISO (2 lettres)",
      "keywords": "angularjs api associer charg code constante de iso lettres nom paneladminapp pays service son"
    },
    {
      "section": "api",
      "id": "panelAdminApp.RESSOURCES",
      "shortName": "panelAdminApp.RESSOURCES",
      "type": "service",
      "moduleName": "panelAdminApp",
      "shortDescription": "Répertorie les variables d&#39;accès aux ressources de l&#39;application",
      "keywords": "acc api application aux de les paneladminapp ressources service variables"
    },
    {
      "section": "api",
      "id": "panelAdminApp.TournoiManager",
      "shortName": "panelAdminApp.TournoiManager",
      "type": "service",
      "moduleName": "panelAdminApp",
      "shortDescription": "Ce manager est chargé de gérer les informations concernant le tournoi",
      "keywords": "api ce charg concernant de est informations le les manager method paneladminapp permet rafraichir service sur tournoi tournoimanager"
    }
  ],
  "apis": {
    "api": true
  },
  "html5Mode": true,
  "editExample": true,
  "startPage": "/api",
  "scripts": [
    "angular.min.js"
  ]
};