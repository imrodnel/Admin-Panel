'use strict';

/**
 * @ngdoc overview
 * @name Application d'administration de la compétition
 * @description
 * Module principal de l'application. Se charge de gérer les vues et controleurs a utiliser en fonction de la route
 */
angular
	.module('panelAdminApp', ['ngRoute', 'ngAnimate', 'ngMessages', 'ui.bootstrap', 'ngDragDrop'])
	.config(['$routeProvider', '$httpProvider', 'apicallProvider', 'RESSOURCES', function ($routeProvider, $httpProvider, apicallProvider, RESSOURCES) {

		// Configuration apiCall
    	apicallProvider.setUrlApi(RESSOURCES.API);
    	apicallProvider.setUrlImages(RESSOURCES.IMAGES);
    	
    	// Intercepteur de vérif. des codes de retour de l'API et injection du token
  		$httpProvider.interceptors.push('intercepteurHttp');

		$routeProvider
			.when('/accueil', {
				templateUrl: 'views/main.html',
				controller: 'MainCtrl'
			})
			.when('/authentification', {
				templateUrl: 'views/authentification.html',
				controller: 'AuthentificationCtrl'
			})
			.when('/equipes', {
				templateUrl: 'views/equipes.html',
				controller: 'EquipesCtrl'
			})
			.when('/joueurs', {
				templateUrl: 'views/joueurs.html',
				controller: 'JoueursCtrl'
			})
			.when('/equipes/:isoEquipe/joueurs', {
				templateUrl: 'views/joueursEquipe.html',
				controller: 'JoueursEquipeCtrl'
			})
			.when('/tirage', {
				templateUrl: 'views/tirage.html',
				controller: 'TirageCtrl'
			})
			.when('/matchs', {
				templateUrl: 'views/matchs.html',
				controller: 'MatchsCtrl'
			})
			// Redirection vers l'accueil
			.otherwise({
				redirectTo: '/accueil'
			}); 
  }])
  .run(function($rootScope, $location, infosConnexion) {
		
		// Décommenter pour activer l'authentification
		$rootScope.$on('$routeChangeStart', function(event, next) {
		
		  // Pas authentifié	
		  if (infosConnexion.token === 'x' || !infosConnexion.login || !infosConnexion.password) {
			
			if(next.templateUrl !== 'views/authentification.html') {

			  $location.path('/authentification');
			}
		  }
		}); 
    });
