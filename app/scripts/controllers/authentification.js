'use strict';

/**
 * @ngdoc controller
 * @name panelAdminApp.controller:AuthentificationCtrl
 * @description
 * Contrôleur de la page d'authentification
 */
angular.module('panelAdminApp')
	.controller('AuthentificationCtrl', ['$scope', '$location', 'apicall', 'infosConnexion', 'informer', function ($scope, $location, apicall, infosConnexion, informer) {
  		

		$scope.lancerConnexion = function(){
			
			apicall.authentification(infosConnexion).success(function(donnees){

				informer.success('Connexion réalisée avec succès');
				infosConnexion.token = donnees.token;
				$location.path('/accueil');
				
			}).error(function(){
			
				informer.error('Erreur lors de la tentative de connexion (veuillez vérifier vos identifiants)');
			});
		};
		
		$scope.authOk = function(){
		
			return ( (infosConnexion.login && infosConnexion.login !== '') && (infosConnexion.password && infosConnexion.password !== '') );
		};
		
		$scope.infosConnexion = infosConnexion;
}]);
