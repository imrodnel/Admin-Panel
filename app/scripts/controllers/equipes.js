'use strict';

/**
 * @ngdoc controller
 * @name panelAdminApp.controller:EquipesCtrl
 * @description
 * Contrôleur de la page gérant les équipes
 */
var app = angular.module('panelAdminApp');

app.controller('EquipesCtrl', ['$scope', '$location', '$modal', 'EquipesManager', function ($scope, $location, $modal, EquipesManager){ 
	
	$scope.manager = EquipesManager;
	
	$scope.infosColonnes = [

		{
			nom:'Drapeau',
			attribut:'URL',
			taille:1,
			optionsColonne:{
			
				triable:false,
				filtrable:false,
				include:true
			},
			template:'views/include/imageTab.html'
		},
		{
			nom:'Équipe',
			attribut:'CompleteName',
			taille:8,
			optionsColonne:{
			
				triable:false,
				filtrable:false
			}
		}
	];
	
	$scope.infosBoutons = [
	
		{
			nom:'Éditer',
			style:{
			
				type:'btn-primary',
				glyph:'glyphicon-edit'
			},
			fonction:function(entree){

				var dialogue = $modal.open({
			
					templateUrl: 'views/modal/editerEquipe.html',
				  	controller: 'EditerEquipeCtrl',
				  	resolve:{
				  	
				  		donnees:function(){
				  		
				  			return {
				  			
				  				entree:entree,
				  			};
				  		}
				  	}
				});
				
				dialogue.result.then(function(equipe) {
					
					equipe.IdTeam = entree.IdTeam; // Rajout de l'identifiant de l'équipe à modifier
					EquipesManager.editerEntree(equipe);
   				});
			}
		},
		{
			nom:'Joueurs',
			style:{
				type:'btn-warning',
				glyph:'glyphicon-th-list'
			},
			fonction:function(equipe){
	
				$location.path('/equipes/'+equipe.NameTeam+'/joueurs');
			}
		},
		{
			nom:'Réinitialiser',
			style:{
			
				type:'btn-danger',
				glyph:'glyphicon-trash'
			},		
			fonction:function(equipe){
	
				var dialogue = $modal.open({
				
					templateUrl: 'views/modal/supprimerEquipe.html',
				  	controller: 'ModalInstanceCtrl',
				  	resolve:{
				  	
				  		donnees:function(){
				  		
				  			return {
				  			
				  				entree:equipe
				  			};
				  		}
				  	}
				});

				dialogue.result.then(function(donnees) {

					EquipesManager.supprimerEntree(donnees.entree);
				});
			}
		}
	];
	
	$scope.optionsTab = {
	
		triable:false,
		editable:false,
		paginable:false,
		filtable:false
	};
}]);
