'use strict';

/**
 * @ngdoc controller
 * @name panelAdminApp.controller:HeaderCtrl
 * @description
 * Contrôleur du header de l'application (utilisé pour actualiser l'affichage de l'onglet actif)
 */
angular.module('panelAdminApp')
	.controller('HeaderCtrl', ['$scope', '$location', 'informer', 'TournoiManager', function ($scope, $location, informer, TournoiManager){
	
	$scope.estActive = function (lienVue) { 
	
    	return $location.path().indexOf(lienVue) === 0;
	};
	
	$scope.tournoiDemarre = function(){
	
		return (TournoiManager.stage !== 'SETTING-UP');
	};
	
	$scope.lienMatch = function(){
	
		if($scope.tournoiDemarre()){
		
			return '#/matchs';
		}
		else{
		
			return '';
		}
	};
	
	TournoiManager.rafraichir();
}]);
