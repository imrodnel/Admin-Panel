'use strict';

/**
 * @ngdoc controller
 * @name panelAdminApp.controller:JoueursEquipeCtrl
 * @description
 * Contrôleur de la page gérant les joueurs d'une équipe
 */
var app = angular.module('panelAdminApp');

app.controller('JoueursEquipeCtrl', ['$scope', '$modal', 'JoueursEquipeManager', function($scope, $modal, JoueursEquipeManager) {
	
	$scope.manager = JoueursEquipeManager;

	$scope.infosColonnes = [
	
		{
			nom : '#',
			attribut : 'IdPlayer',
			taille : 1,
			optionsColonne : {
			
				triable : true,
				filtrable : false
			},
			triage : {
			
				nom : 'Id'
			}
		},
		{
			nom : 'Prénom',
			attribut:'FirstName',
			taille : 1,
			optionsColonne : {
			
				triable : true,
				filtrable : true
			},
			triage : {
			
				nom : 'Prénom'
			},
			filtrage : {
			
				nom : 'Prénom',
				taille : 6
			}
		},
		{
			nom : 'Nom',
			taille : 3,
			attribut : 'Name',
			optionsColonne : {
			
				triable : true,
				filtrable : true
			},
			triage : {
			
				nom : 'Nom'
			},
			filtrage : {
			
				nom : 'Nom',
				taille : 6
			}
		},
		{
			nom : 'Sexe',
			taille : 1,
			attribut : 'Sex',
			optionsColonne : {
			
				triable : true,
				filtrable : false
			},
			triage : {
			
				nom : 'Sexe'
			}
		},
		{
			nom : 'Date de naissance',
			attribut : 'DateBirth',
			taille : 2,
			optionsColonne : {
			
				triable : true,
				filtrable : false
			},
			triage : {
			
				nom : 'Date de naissance'
			}
		}
	];
	
	$scope.infosBoutons = [
	
		{
			nom : 'Éditer',
			style : {
				type : 'btn-primary',
				glyph : 'glyphicon-edit'
			},
			fonction: function(joueur){

				var dialogue = $modal.open({
			
					templateUrl: 'views/modal/editerJoueur.html',
				  	controller: 'EditJoueurCtrl',
				  	resolve:{
				  	
				  		donnees:function(){
				  		
				  			return {
				  			
				  				entree:joueur
				  			};
				  		}
				  	}
				});
				
				dialogue.result.then(function(joueur){

					JoueursEquipeManager.editerEntree(joueur);
				});
			}
		},
		{
			nom : 'Photo',
			style : {
				type : 'btn-warning',
				glyph : 'glyphicon-camera'
			},
			fonction: function(joueur){
				
				$modal.open({
				
					templateUrl: 'views/modal/showPhoto.html',
				  	controller: 'ShowPhotoCtrl',
				  	resolve:{
				  	
				  		donnees:function(){
				  		
				  			return {
				  			
				  				entree:joueur
				  			};
				  		}
				  	}
				});
			}
		},
		{
			nom : 'Supprimer',
			style : {
				type : 'btn-danger',
				glyph : 'glyphicon-trash'
			},
			fonction : function(joueur){
				
				var dialogue = $modal.open({
				
					templateUrl: 'views/modal/supprimerJoueur.html',
				  	controller: 'SupprimerJoueurCtrl',
				  	resolve:{
				  	
				  		donnees:function(){
				  		
				  			return {
				  			
				  				entree:joueur
				  			};
				  		}
				  	}
				});

				dialogue.result.then(function(joueur){
					
					JoueursEquipeManager.supprimerEntree(joueur);
				});
			}
		}
	];
	
	$scope.optionsTab = {
	
		triable : true,
		filtrable : true, 
		paginable : true,
		editable : true,
		edition:{
		
			fonctionAjout: function(){
			
				var dialogue = $modal.open({
				
					templateUrl: 'views/modal/ajoutJoueurEquipe.html',
				  	controller: 'AjoutJoueurEquipeCtrl',
				});

				dialogue.result.then(function(joueur){
					
					JoueursEquipeManager.ajouterEntree(joueur);
				});
			}
		}
	};
}]);
