'use strict';

/**
 * @ngdoc controller
 * @name panelAdminApp.controller:MainCtrl
 * @description
 * Contrôleur de la page d'accueil
 */
angular.module('panelAdminApp')
	.controller('MainCtrl', ['$scope', 'informer', 'apicall', '$filter', '$q', 'chargementGlobal', 'EquipesManager', 'JoueursManager', 'TournoiManager', function($scope, informer, apicall, $filter, $q, chargementGlobal, EquipesManager, JoueursManager, TournoiManager){
		
		var nbRencontresKo;
		
		$scope.nbJoueurs = function(){return JoueursManager.entrees.length;};
		
		$scope.equipesOk = function(){
		
			return ($filter('filter')(EquipesManager.entrees, {CompleteName:null})).length === 0;
		};
		
		$scope.joueursEquipesOk = function(){
			
			// Pour chaque équipe
			for(var i=0; i<EquipesManager.entrees.length; i++){
			
				if(EquipesManager.entrees[i].CompleteName !== null){
				
					var joueursEquipe = $filter('filter')(JoueursManager.entrees, {Team:EquipesManager.entrees[i].NameTeam});
					
					var nbHommes = ($filter('filter')(joueursEquipe, {Sex:'M'})).length;
					var nbFemmes = ($filter('filter')(joueursEquipe, {Sex:'W'})).length;
					
					if( (nbHommes < 2) || (nbFemmes < 2) ){
					
						return false;
					}
				}
			}
			
			return true;
		};
		
		$scope.tirageOk = function(){
		
			return nbRencontresKo === 0;
		};
		
		$scope.changerEtapeTournoi = TournoiManager.etapeSuivante;
		
		$scope.changerEtapeOk = function(){
		
			return ($scope.equipesOk() && $scope.joueursEquipesOk() && $scope.tirageOk() && TournoiManager.stage !== 'Final');
		};
		
		$scope.rafraichir = function(){
		
			var promises = [apicall.getTirage()];
			TournoiManager.rafraichir();
			EquipesManager.rafraichir();
			JoueursManager.rafraichir();
		
			chargementGlobal.active = true;
		
			$q.all(promises).then(function(donnees){

				nbRencontresKo = ($filter('filter')(donnees[0].data.matches, {Team_A:null})).length;
				$scope.stadeTournoi = TournoiManager.stage;
				
				chargementGlobal.active = false;
			
			}, function(){
		
				informer.error('Impossible de charger les informations');
			});
		};
		
		$scope.rafraichir();
	}]);
