'use strict';

/**
 * @ngdoc controller
 * @name panelAdminApp.controller:MatchsCtrl
 * @description
 * 
 * Controleur de la page de gestion des matchs
 */
angular.module('panelAdminApp')
	.controller('MatchsCtrl', ['$scope', 'apicall', 'informer', 'chargementGlobal', '$q', '$location', 'RESSOURCES', function ($scope, apicall, informer, chargementGlobal, $q, $location, RESSOURCES) {
		
		var stadeCourant;
		
		$scope.entreesTours = [
		
			{
				nom:'Courant',
				valeur:'CURRENT'
				
			},
			{
				nom:'Quart de finale',
				valeur:'1/4'
			},
			{
				nom:'Demi-finale',
				valeur:'1/2'
			},
			{
				nom:'Finale',
				valeur:'Final'
			}
		];
		
		$scope.choixTour = {selection:$scope.entreesTours[0]};
		
		$scope.$watch('choixTour.selection', function(nouveau, ancien){
			
			if(nouveau !== ancien){
			
				chargerSuperMatchs($scope.choixTour.selection.valeur);
			}
		});
		
		var chargerSuperMatchs = function(choixTour){
		
			chargementGlobal.active = true;
			
			var promises = [apicall.getSuperMatchs(choixTour), apicall.getTournoi()];
			
			$q.all(promises).then(function(donnees){
			
				$scope.superMatchs = donnees[0].data.supermatches;
				stadeCourant = donnees[1].data.tournament.Stage;
		
				for(var i = $scope.entreesTours.length-1; $scope.entreesTours[i].valeur !== stadeCourant && i>0; i--){
					$scope.entreesTours.splice(i, 1);
				}
				
				chargementGlobal.active = false;
			
			}, function(){
			
				informer.error('Erreur dans la récupération des informations');
				chargementGlobal.active = false;
			});
		};
		
		$scope.getExport = function(){
		
			var lien = RESSOURCES.API + '/exports/matches?round=';
			
			if($scope.choixTour.selection.valeur === 'CURRENT'){
			
				lien += stadeCourant;
			}
			else{
			
				lien += $scope.choixTour.selection.valeur;
			}
			
			return lien;
		};
		
		chargerSuperMatchs($scope.choixTour.selection.valeur);
	}]);
