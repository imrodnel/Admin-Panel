'use strict';
/**
 * @ngdoc controller
 * @name panelAdminApp.controller:AjoutJoueurCtrl
 * @description
 * Contrôleur de fenêtre modal servant à saisir les informations nécessaires à l'ajout d'un joueur
 */

var app = angular.module('panelAdminApp');

app.controller('AjoutJoueurCtrl', ['$scope','$modalInstance', '$filter', 'apicall', 'informer', function ($scope, $modalInstance, $filter, apicall, informer) {
	
	$scope.joueur = {
	
		DateBirth: new Date()
	};
	
	$scope.$watch('joueur.DateBirth', function(){
		
		$scope.joueur.DateBirth = $filter('date')($scope.joueur.DateBirth, 'yyyy-MM-dd');
	});
	
	apicall.getEquipes().success(function(data){
	
		$scope.equipes = data.teams;
		
	}).error(function(){
	
		informer.error('Impossible de récupérer les équipes');
	});

	$scope.today = new Date();
	
	$scope.ok = function () {

		$modalInstance.close($scope.joueur);
	};

	$scope.cancel = function () {

		$modalInstance.dismiss('cancel');
	};
}]);
