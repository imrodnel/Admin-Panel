'use strict';

/**
 * @ngdoc controller
 * @name panelAdminApp.controller:AjoutJoueurEquipeCtrl
 * @description
 * Contrôleur de fenêtre modal servant à saisir les informations nécessaires à l'ajout d'un joueur dans une équipe
 */

var app = angular.module('panelAdminApp');

app.controller('AjoutJoueurEquipeCtrl', ['$scope', '$modalInstance', '$filter', '$routeParams', function ($scope, $modalInstance, $filter, $routeParams) {

	$scope.equipe = $routeParams.isoEquipe;

	$scope.joueur = {
	
		DateBirth : new Date(),
		Team: $scope.equipe
	};
	
	$scope.$watch('joueur.DateBirth', function(){
		
			$scope.joueur.DateBirth = $filter('date')($scope.joueur.DateBirth, 'yyyy-MM-dd');
	});
	
	$scope.today = new Date();
	
	$scope.ok = function() {
		
		$modalInstance.close($scope.joueur);
	};

	$scope.cancel = function() {

		$modalInstance.dismiss('cancel');
	};
}]);
