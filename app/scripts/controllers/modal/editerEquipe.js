'use strict';

/**
 * @ngdoc controller
 * @name panelAdminApp.controller:EditerEquipeCtrl
 * @description
 * Contrôleur de fenêtre modal servant à éditer une équipe
 */

var app = angular.module('panelAdminApp');

app.controller('EditerEquipeCtrl', ['$scope','$modalInstance', 'donnees', function ($scope, $modalInstance, donnees) {

	$scope.equipe = angular.copy(donnees.entree);
	
	$scope.ok = function () {

		$modalInstance.close($scope.equipe);
	};

	$scope.cancel = function () {

		$modalInstance.dismiss('cancel');
	};
}]);
