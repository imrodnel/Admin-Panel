'use strict';

/**
 * @ngdoc controller
 * @name panelAdminApp.controller:EditerEquipeCtrl
 * @description
 * Contrôleur de fenêtre modal servant à éditer un joueur
 */
 
angular.module('panelAdminApp')
  .controller('EditJoueurCtrl', ['$scope', '$modalInstance', '$filter', 'informer', 'apicall', 'donnees', function ($scope, $modalInstance, $filter, informer, apicall, donnees) {
    
		$scope.joueur = angular.copy(donnees.entree);
		
		$scope.$watch('joueur.DateBirth', function(){
		
			$scope.joueur.DateBirth = $filter('date')($scope.joueur.DateBirth, 'yyyy-MM-dd');
		});
		
		apicall.getEquipes().success(function(data){
		
			$scope.equipes = data.teams;
			
		}).error(function(){
		
			informer.error('Erreur lors de l\'appel à l\'API');
		});
		
		$scope.today = new Date();
		
		$scope.ok = function () {
		
			$modalInstance.close($scope.joueur);
		};

		$scope.cancel = function () {

			$modalInstance.dismiss('cancel');
		};
  }]);
