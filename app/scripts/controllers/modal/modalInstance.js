'use strict';

/**
 * @ngdoc controller
 * @name panelAdminApp.controller:ModalInstanceCtrl
 * @description
 * Contrôleur de fenêtre modal servant à lancer une fenêtre modal sans interaction particulière
 */
 
angular.module('panelAdminApp')

	.controller('ModalInstanceCtrl', ['$scope', '$modalInstance', 'donnees', function ($scope, $modalInstance, donnees) {

		$scope.donnees = donnees;
		
		$scope.ok = function () {

			$modalInstance.close();
		};

		$scope.cancel = function () {

			$modalInstance.dismiss();
		};
}]);
