'use strict';

/**
 * @ngdoc controller
 * @name panelAdminApp.controller:ShowPhotoCtrl
 * @description
 * Permet d'afficher la photo d'un joueur dans un modal
 */
angular.module('panelAdminApp')
  .controller('ShowPhotoCtrl', ['$scope', '$modalInstance', 'donnees', 'RESSOURCES', function ($scope, $modalInstance, donnees, RESSOURCES) {
   
	$scope.joueur = donnees.entree;
	
	// Photo par défaut
	if($scope.joueur.Url === null){
	
		$scope.joueur.Url = RESSOURCES.IMAGES + '/photos/default.png';
	}
	
	$scope.joueur.Url += '?nocache=' + Math.random();
	
	$scope.close = function () {
		
		$modalInstance.close();
	};
}]);
