'use strict';

/**
 * @ngdoc controller
 * @name panelAdminApp.controller:ModalSupprimerjoueurCtrl
 * @description
 * # ModalSupprimerjoueurCtrl
 * Controller of the administrationAceCreamApp
 */
angular.module('panelAdminApp')
	.controller('SupprimerJoueurCtrl', ['$scope', '$modalInstance', 'donnees', function($scope, $modalInstance, donnees){

		$scope.joueur = donnees.entree;

		$scope.ok = function () {

			$modalInstance.close($scope.joueur);
		};

		$scope.cancel = function () {

			$modalInstance.dismiss();
		};
	}]);
