'use strict';

/**
 * @ngdoc controller
 * @name panelAdminApp.controller:TerminerMatchCtrl
 * @description
 * Modal servant à saisir les informations nécessaires pour terminer un match
 */
angular.module('panelAdminApp')
	.controller('TerminerMatchCtrl', ['$scope', '$modalInstance', '$filter', 'donnees', function ($scope, $modalInstance, $filter, donnees){
	
		$scope.match = angular.copy(donnees.entree);
		$scope.match.endDate = new Date();
		
		$scope.choixGagnant = null;
		$scope.entreesEquipes = [
		
			{
				nom:$scope.match.NameTeam_A,
				valeur:'A'
			},
			{
				nom:$scope.match.NameTeam_B,
				valeur:'B'
			}
		];
		
		$scope.changerGagnant = function(entree){
			
			$scope.choixGagnant = entree.nom;
			$scope.match.winner = entree.valeur;
		};
		
		$scope.ok = function () {
		
			$scope.match.endDate = $filter('date')($scope.match.endDate, 'yyyy-MM-dd HH:mm:ss');
			$modalInstance.close($scope.match);
		};

		$scope.cancel = function () {

			$modalInstance.dismiss('cancel');
		};
	}]);
