'use strict';

/**
 * @ngdoc controller
 * @name panelAdminApp.controller:TirageCtrl
 * @description
 * Contrôleur de la page de gérant l'initialisation des rencontres via tirage
 */
angular.module('panelAdminApp')
  .controller('TirageCtrl', ['$scope', '$filter', 'apicall', 'informer', 'chargementGlobal', function ($scope, $filter, apicall, informer, chargementGlobal) {
	
	chargementGlobal.active = true;
	
	// Récupération des équipes
	apicall.getEquipes().success(function(donnees){
	
		$scope.equipes = donnees.teams;
	
		// Récupération du tirage
		apicall.getTirage().success(function(donnees){
	
			$scope.tirage = [];
			var tirageTemp = $filter('applatirTirage')(donnees.matches);
			
			// Association des objets équipes aux entrées du tirage + suppression dans $scope.equipe
			for(var i = $scope.equipes.length - 1; i >= 0; i--) {
			
				var indexTirages = tirageTemp.indexOf($scope.equipes[i].NameTeam);
				
				if(indexTirages !== -1){
				
					$scope.tirage[indexTirages] = $scope.equipes[i];
					$scope.equipes.splice(i, 1);
				}
			}
			
			chargementGlobal.active = false;
			
			$scope.$watchCollection('tirage', function(oldTirage, newTirage){
				
				if(!angular.equals(oldTirage, newTirage) && checkTirage()){
			
					apicall.setTirage($scope.tirage).success(function(){

						informer.success('Tirage mis à jour avec succès');

					}).error(function(){

						informer.error('La mise à jour du tirage a rencontré une erreur');
					});
				}
			});
		
		}).error(function(){
	
			informer.error('Impossible de récupérer les tirages');
		});
		
	}).error(function(){
	
		informer.error('Impossible de récupérer la liste des équipes');
	});
	
	$scope.objectEmpty = function(obj){
	
		if(obj){
		
			return Object.keys(obj).length === 0;
		}
		
		return true;
	};
	
	var checkTirage = function(){
	
		for(var i=0; i<8; i+=1){
			
			if($scope.objectEmpty($scope.tirage[i])){
				return false;
			}
		}
		
		return true;
	};
}]);
