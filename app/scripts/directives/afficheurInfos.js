'use strict';

 /**
     * @ngdoc directive
     * @name panelAdminApp.directive:afficheurInfos
     * @scope
     * @restrict E
     *
     * @link panelAdminApp.service:informer
     
     * @description
     * Cette directive agit en collaboration avec le service informer et est chargée de générer le code HTML d'affichage des notifications au sein de l'application
     */
angular.module('panelAdminApp')
  .directive('afficheurInfos', ['informer', function (informer) {
    return {
      templateUrl: 'views/directives/afficheurInfos.html',
      restrict: 'E',
      scope:{},
      replace:true,
      link: function($scope) {
      
			$scope.alertes = informer.getInformations();
			
			$scope.fermerAlerte = function(alerte){
			
				informer.supprimer(alerte);
			};
      }
    };
  }]);
