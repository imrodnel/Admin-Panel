'use strict';

 /**
     * @ngdoc directive
     * @name panelAdminApp.directive:chargement
     * @scope
     * @restrict A
     *
     * @param {Boolean}  statusChargement  Un booléen charger de désigner si le contenu englobé est en chargement (true) ou non (false)
     * @param {Boolean}  local  Spécifie si l'animation de chargement doit prendre tout l'écran ou juste le container parent
     *
     * @description
     * Cette directive est chargée d'afficher une animation de chargement
     */
var app = angular.module('panelAdminApp');

app.directive('chargement', ['chargementGlobal', function(chargementGlobal) {
		
	return {
	
		// Affichage du chargeur selon l'état de la variable chargement
		template: '<div><div id="loader-wrapper" class="anim-loader" ng-class="{\'loader-wrapper-full\':!local}" ng-if="chargement.active"><div id="loader" ng-class="{\'loader-full\':!local}"></div></div><div ng-transclude></div></div>',
		
		//On remplace le contenu original qu'on place dans un container temporaire
		restrict: 'A',		
		transclude: true,
		replace: true,

		scope:{

        	chargement: '=',
        	local: '='
     	},
     	link:function($scope){
     	
     		if(!$scope.local){
     		
     			$scope.chargement = chargementGlobal;
     		}
     	}
	};
}]);
