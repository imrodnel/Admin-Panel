'use strict';

/**
 * @ngdoc directive
 * @name panelAdminApp.directive:fileModel
 * @description
 * Cette directive est charger de surveiller un input de type file car celui-ci n'est pas pris en charge par AngularJS
 */
angular.module('panelAdminApp').directive('fileModel', [function () {
    return {
        restrict: 'A',
        scope:{
        
        	fichier:'=fileModel'
        },
        link: function($scope, element) {
            
            var changementFile = function (changeEvent) {
				
				$scope.fichier = changeEvent.target.files[0];
			};
			
			element.bind('change', changementFile);
			
			$scope.$on('$destroy', function(){
			
				element.unbind('change', changementFile);
			});
        }
    };
}]);
