'use strict';

 /**
     * @ngdoc directive
     * @name panelAdminApp.directive:flagSelector
     * @scope
     * @restrict E
     *
     * @param {Object}  selection  (read-only) Le pays sélectionné actuellement
     *
     * @description
     * Cette directive est chargée d'afficher une liste de pays avec drapeaux associés et la possibilité d'en sélectionner un
     */
angular.module('panelAdminApp')
  .directive('flagSelector', ['$filter', 'pays', 'RESSOURCES', function ($filter, pays, RESSOURCES) {
		return {
			  templateUrl: 'views/directives/flagSelector.html',
			  restrict: 'E',
			  
			  scope: {
			  
					elemSelection:'='
			  },
			  replace:true,
			  link: function($scope){
			  	
				  	$scope.selectionnerEquipe = function(equipe){
				  		
				  		if($scope.oldSelection){
				  		
				  			$scope.oldSelection.selected  = false; 
				  		}
				  		
				  		equipe.selected = true;
				  		$scope.oldSelection = equipe;
				  		
				  		$scope.elemSelection = {
				  		
				  			CompleteName : equipe.NomPays,
				  			NameTeam : equipe.Iso,
				  			URL : RESSOURCES.DRAPEAUX + $filter('lowercase')(equipe.Iso) + '.png'
				  		};
			 	  };
			  
				  $scope.equipes = angular.copy(pays); // Prévention des modifications
				  $scope.oldSelection = null;
			  }
		};
	}]
);
