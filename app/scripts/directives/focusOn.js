'use strict';

 /**
     * @ngdoc directive
     * @name panelAdminApp.directive:focusOn
     * @scope
     * @restrict A
     *
     * @param {*}  elemFocus  Un element à surveiller afin de prendre le focus à chaque changement
     *
     * @description
     * Cette directive est chargée de déclencher le focus de l'élément sur lequel elle est située lorsque elemFocus change
     */
angular.module('panelAdminApp')
	.directive('focusOn', ['$timeout', function($timeout) {
	
		return {
		
			restrict:'A',
			
			scope: { 
			
				trigger: '=focusOn'
				
			},
			
			link: function($scope, element, attrs) {
				
				// Si on a un ng-repeat, il faut alors spécifier un attribut position pour focuser seulement sur le premier élément
				if(attrs.position === undefined || $scope.$eval(attrs.position) === true){
					
					// On surveille le trigger pour mettre l'autofocus en cas de besoin
					$scope.$watch('trigger', function(nouvelleValeur, ancienneValeur) {
				
						if(nouvelleValeur === true && nouvelleValeur !== ancienneValeur) { 
						
							$timeout(function() {element[0].focus();});
						}
						else{
					
							$timeout(function() {element[0].blur();});
						}
					});
				}
			}
		};
	}]);
