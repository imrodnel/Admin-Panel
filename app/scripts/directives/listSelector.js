'use strict';

/**
 * @ngdoc directive
 * @name panelAdminApp.directive:listSelector
 *
 * @description
 * Cette directive est chargée de générer un bouton imitant le type select 
 *
 * @param {Array}  entrees Les entrées du select
 * @param {String} champsNom Le champs des objets utilisé pour l'affichage
 * @param {String} design	Une ou plusieures classes CSS à appliquer à l'élement
 * @param {Object} choix L'objet choisi par l'utilisateur
 */
angular.module('panelAdminApp')
	.directive('listSelector', function () {
		
		return{
		
			templateUrl: 'views/directives/listSelector.html',
			restrict: 'E',

			scope: {

				entrees:'=',
				champsNom:'@',
				design:'@',
				choix:'='
			},
			replace:true,
			link: function($scope){
				
				$scope.changerChoix = function(entree){
			
					$scope.choix.selection = entree;
				};
			}
		};
	});
