'use strict';

/**
 * @ngdoc directive
 * @name panelAdminApp.directive:ongletMatch
 * @scope
 *
 * @param {Object} match Le match auquel l'onglet doit faire référence
 *
 * @description
 * Utilisant le système d'ascenceur pour l'affichage des matchs, ongletMatch fournit une marche de cet ascenceur (un onglet dépliable) permettant de modifier le status et les informations d'un match
 */
angular.module('panelAdminApp')
	.directive('ongletMatch', ['$q', 'apicall', 'informer', '$modal', 'chargementGlobal', function($q, apicall, informer, $modal, chargementGlobal){
	
		return {
		
			templateUrl: 'views/directives/ongletMatch.html',
			restrict: 'E',
			scope:{
			
				match:'='
			},
			replace:true,
			link: function postLink($scope){
				
				var copieMatch = {};
				
				var estModifie = function(input){
					
					if($scope.formulaireMatch[input]){
					
						return $scope.formulaireMatch[input].$dirty;
					}
				};
				
				$scope.actionMaj = function(){
					
					var promises = [];
					var idMatch = $scope.match.id;
					
					// Changement des joueurs
					if(estModifie('joueurA1')){
						promises.push(apicall.matchChangerJoueur(idMatch, copieMatch.playerA1Id, $scope.match.playerA1Id));
					}
					
					if(estModifie('joueurB1')){
						promises.push(apicall.matchChangerJoueur(idMatch, copieMatch.playerB1Id, $scope.match.playerB1Id));
					}
					
					if($scope.matchDouble()){
					
						if(estModifie('joueurA2')){
							promises.push(apicall.matchChangerJoueur(idMatch, copieMatch.playerA2Id, $scope.match.playerA2Id));
						}
						
						if(estModifie('joueurB2')){
							promises.push(apicall.matchChangerJoueur(idMatch, copieMatch.playerB2Id, $scope.match.playerB2Id));
						}
					}
					
					// Màj du court
					if(estModifie('court')){
						promises.push(apicall.changeMatchCourt($scope.match.id, $scope.match.court));
					}
					
					// Match terminé => Màj du score & dateFin & winner
					if(matchTermine()){
						
						var changed = false;
						for(var i=1; i<6; i++){
						
							if(estModifie('set'+i+'A') || estModifie('set'+i+'B')){
							
								changed = true;
								break;
							}
						}

						promises.push(apicall.changeEndMatch($scope.match.id, $scope.match.score, $scope.match.winner, $scope.match.endDate));
					}
					
					$q.all(promises).then(function(){
					
						informer.success('Le match a été mis à jour avec succès');
						updateInfos();
					
					}, function(){
					
						informer.error('Impossible de mettre à jour le match');
						updateInfos();
					});
				};
				
				var actionAnnuler = function(){
				
					apicall.cancelMatch($scope.match.id).success(function(){
					
						informer.success('Le match a été annulé avec succès');
						updateInfos();
						
					}).error(function(){
					
						informer.error('Impossible d\'annuler le match');
					});
				};
				
				var actionDemarrer = function(){
					
					apicall.startMatch($scope.match).success(function(){
					
						informer.success('Le match a été démarré avec succès');
						updateInfos();
					
					}).error(function(){
					
						informer.error('Impossible de démarrer le match');
					});
				};
				
				var actionTerminer = function(){
				
					var dialogue = $modal.open({
			
						templateUrl: 'views/modal/terminerMatch.html',
					  	controller: 'TerminerMatchCtrl',
					  	resolve:{
				  	
					  		donnees:function(){
					  		
					  			return {entree:$scope.match};
					  		}
				  		}
					});
				
					dialogue.result.then(function(match) {
						
						apicall.changeEndMatch(match.id, match.score, match.winner, match.endDate).success(function(){
							informer.success('Match terminé avec succès');
							updateInfos();
						
						}).error(function(){
					
							informer.error('Impossible de terminer le match');
		   			 	});
	   				});
				};
				
				$scope.matchDouble = function(){
					
					return ($scope.match.category.substr(0, 1) === 'D');
				};
				
				var matchTermine = function(){
				
					return ($scope.match.status === 'FINISH');
				};
				
				var matchAnnule = function(){
				
					return ($scope.match.status === 'CANCEL');
				};
				
				var matchEnCours = function(){
				
					return ($scope.match.status === 'PROGRESS');
				};
				
				$scope.matchAVenir = function(){
				
					return ($scope.match.status === 'SOON');
				};
				
				var genreMatch = function(){
				
					return ($scope.match.category.substr(1, 1));
				};
				
				var matchFeminin = function(){
				
					return ($scope.match.category.substr(1, 1) === 'W');
				};
				
				var matchMasculin = function(){
				
					return ($scope.match.category.substr(1, 1) === 'M');
				};
				
				$scope.formatDate = /^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/i;
				
				$scope.design = {
				
					'SOON' : {
					
						header : {
						
							message : 'À venir',
							design : 'default'
						},
						boutonA : {
						
							texte:'Démarrer',
							action:actionDemarrer,
							design:'success'
						},
						boutonB : {
						
							texte:'Annuler',
							action:actionAnnuler,
							design:'danger'
						}
					},
					'PROGRESS' : {
					
						header : {
							message : 'En cours',
							design : 'primary'
						},
						boutonA : {
						
							texte:'Terminer',
							action:actionTerminer,
							design:'danger'
						}
					},
					'FINISH' : {
					
						header : {
						
							message : 'Terminé',
							design : 'success'
						}
					}, 
					'CANCEL' : {
					
						header : {
						
							message : 'Annulé',
							design : 'warning'
						}
					}
				};
				
				$scope.titre = function(){
					
					var message = '';
					
					if($scope.matchDouble()){
					
						message += 'Double ';
					}
					else{
					
						message += 'Simple ';
					}
					
					if(matchMasculin()){
					
						message += 'homme';
					}
					else if(matchFeminin()){
					
						message += 'femme';
					}
					else{
					
						message += 'mixte';
					}
					
					return message;
				};
				
				$scope.optionsCal = {
				
					'show-weeks':false,
					'starting-day':1
				};
				
				$scope.nomJoueur = function(entree){
				
					return(entree.Name + ' ' + entree.FirstName);
				};
				
				/* Récupuration des infos du match et mise à jour via l'API */
				var updateInfos = function(){
				
					var idMatch = $scope.match.id;
					chargementGlobal.active = true;
					
					apicall.getMatch(idMatch).then(function(donnees){
					
						donnees.id = idMatch;
						$scope.match = donnees;
						copieMatch = angular.copy($scope.match);
						
						// Chargement des infos pour éditions/consultation
						var promises = [];
						
						promises.push(apicall.getCourtsDispo());
						promises.push(apicall.getJoueursEquipe($scope.match.teamA, genreMatch()));
						promises.push(apicall.getJoueursEquipe($scope.match.teamB, genreMatch())); 
						promises.push(apicall.getScore($scope.match.id));
						
						$q.all(promises).then(function(donnees){
						
							$scope.entreesCourt = donnees[0].data.courts;
							if($scope.match.court){
							
								$scope.entreesCourt.push($scope.match.court);
							}
							
							$scope.listeJoueursA1 = angular.copy(donnees[1].data.players);
							$scope.listeJoueursA2 = angular.copy(donnees[1].data.players);
							$scope.listeJoueursB1 = angular.copy(donnees[2].data.players);
							$scope.listeJoueursB2 = angular.copy(donnees[2].data.players);
							
							// On récupère le score et on complète avec des -1 pour les jeux non-joués
							$scope.match.score = donnees[3].data.sets;
							for(var i = $scope.match.score.length; i < 5; i++){
							
								$scope.match.score[i] = {
									A:-1,
									B:-1
								};
							}
							
							/* Màj pour le vainqueur */
							$scope.entreesEquipes = [
		
								{
									nom:$scope.match.NameTeam_A,
									valeur:'A'
								},
								{
									nom:$scope.match.NameTeam_B,
									valeur:'B'
								}
							];
							
							if($scope.match.winner === 'A'){
							
								$scope.choixGagnant = $scope.entreesEquipes[0].nom;
							}
							else{
							
								$scope.choixGagnant = $scope.entreesEquipes[1].nom;
							}
							
							chargementGlobal.active = false;
							
						}, function(){
						
							informer.error('Erreur dans le chargement du match');
						});
						
					}, function(){
					
						informer.error('Impossible de récupérer les informations du match');
					});
				};
		
				$scope.changerGagnant = function(entree){
			
					$scope.choixGagnant = entree.nom;
					$scope.match.winner = entree.valeur;
				};
				
				$scope.ouvert = false;
				$scope.$watch('ouvert', function(){
					
					if($scope.ouvert === true){

						updateInfos();
					}
				});
			}
		};
	}]);
