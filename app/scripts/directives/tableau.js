'use strict';

/**
 * @ngdoc directive
 * @name panelAdminApp.directive:tableau
 * @description Directive définissant un tableau éditable à plusieurs entrées
 
 * @param {Object} manager Le manager chargé de gérer les entrées du tableau
 
 * @param {Array} infosColonnes Ce tableau d'objets recense les informations sur les colonnes, il est de la forme suivante :
 	- nom
 	- attribut
 	- taille Au sens de bootstrap, c'est à dire que le total des colonnes ne doit pas exceder 9 si le tableau est éditable, 12 sinon
 	- optionsColonne
 		- triable
 		- filtrable
 		- include Spécifie si on doit représenter l'entrée dans un template
 	- triage
 		- nom
 	- filtrage
 		- nom
 		- taille La taille du filtre, le total de la taille de entrées filtrables de doit pas excéder 12
 	- template Le template à utiliser pour réprésenter l'entrée
 
 * @param {Array} infosBoutons Ce tableau d'objets recense les informations sur les bontons d'édition/consultation associés à chaque entrée, il est de la forme suivante :
 	- nom
	- style
		- type La classe de bouton bootstrap à associer au bouton
		- glyph La classe glyphicon bootstrap à associer au bouton
		- fonction La fonction a exécuter lors du clic sur le bouton
		
 * @param {Object} optionsTab Cet objet définit les options à activer dans le tableau :
 	- triable
 	- filtrable
 	- paginable
 	- editable
 	- edition Une fonction qui s'exécute lorsqu'on clique sur un des boutons pour ajouter une entrée
 
 */
angular.module('panelAdminApp')
  .directive('tableau', ['$filter', 'informer', 'RESSOURCES', function ($filter, informer, RESSOURCES) {
    return {
    
    	restrict: 'E',
    	scope:{
    	
			manager : '=',
			infosColonnes : '=',
			infosBoutons : '=',
    		optionsTab : '='
    	},
    	replace:true,
		templateUrl: 'views/directives/tableau.html',
		
		link: function($scope) {
			
			/* Gestion du triage */
			$scope.trierPar = function(entree){

				$scope.entreeTriSelection = entree;
			};
			
			$scope.inverserTri = function(){
			
				$scope.sensTri  = !$scope.sensTri;
			};
			
			$scope.sensTri = false;
			$scope.entreeTriSelection = $filter('filter')($scope.infosColonnes, {optionsColonne:{triable:true}})[0];
			
			/* Gestion des filtres du tableau */
			$scope.affichageFiltres = false;
			$scope.recherche = {};
			
			$scope.changerAffichageFiltres = function(){
			
				$scope.affichageFiltres = !$scope.affichageFiltres;
			};
			
			$scope.tailleFiltres = function(){
				
				return Math.ceil(($scope.infosColonnes.length+$scope.infosBoutons.length+$scope.optionsTab.selectionnable)/$filter('filter')($scope.infosColonnes, {optionsColonne : {filtrable:true}}).length);
			};
			
			/* Gestion des entrées */
			$scope.tableauVide = function(){
			
				return $scope.totalElements === 0;
			};
	
			/* Gestion de la pagination */
			var toutesEntrees = {nom:'Toutes', valeur:0};
			
			$scope.entreesPages = [

				{nom:'10', valeur:10},
				{nom:'20', valeur:20},
				{nom:'30', valeur:30},
				{nom:'40', valeur:40},
				{nom:'50', valeur:50},
				toutesEntrees
			];
			
			if($scope.optionsTab.paginable){

				$scope.choixEntreesPages = {selection:$scope.entreesPages[0]};
			}
			else{
			
				$scope.choixEntreesPages = {selection:toutesEntrees};
			}
			
			$scope.$watchCollection('manager.entrees', function(){
			
				var taille = $scope.manager.entrees.length;
				$scope.totalElements = taille;
				toutesEntrees.valeur = taille;
			});
			
			$scope.pageCourante = {valeur:1}; // Passage par objet pour la directive pagination d'Angular UI
	
			// Récupère l'indice de la première entrée à afficher dans le tableau
			$scope.premiereEntree = function(userFriendly){
		
				return ($scope.pageCourante.valeur-1)*$scope.choixEntreesPages.selection.valeur + ($scope.totalElements !== 0 && userFriendly);
			};
	
			// Récupère l'indice de la dernière entrée à afficher dans le tableau
			$scope.derniereEntree = function(){
		
				var dern = $scope.pageCourante.valeur*$scope.choixEntreesPages.selection.valeur;
				var res;
				
				if(dern < $scope.totalElements){
		
					res = dern;
				}
				else{
		
					res = dern-(dern-$scope.totalElements);
				}

				return res;
			};
			
			$scope.genererDrapeau = function(lien){
			
				if(lien){
				
					return lien;
				}
				// Drapeau par défaut
				else{
				
					return (RESSOURCES.DRAPEAUX + 'default.png');
				}
			};

			$scope.tailleFiltres = function(){
				
				return Math.ceil(($scope.infosColonnes.length+$scope.optionsTab.editable)/$filter('filter')($scope.infosColonnes, {optionsColonne:{filtrable:true}}).length);
			};			
			
			$scope.tableauVide = function(){
			
				return $scope.manager.entrees.length === 0;
			};
			
			$scope.rafraichir = function(){
			
				$scope.manager.rafraichir();	
			};
			
			$scope.entrees = $scope.manager.entrees;
			
			$scope.manager.rafraichir();
        }
    };
  }]);
