'use strict';

/**
 * @ngdoc filter
 * @name panelAdminApp.filter:applatirScore
 * @function
 * @param {Array} source L'objet contenant les scores
 * @description
 * Filtre qui permet de préparer un objet contenant le score d'un match pour appel à l'API
 */
angular.module('panelAdminApp')
	.filter('applatirScore', function () {
	
		return function (score) {
		
			var res = {};
			
			for(var i = 0; i<5; i++){
				
				var set = 'set'+(i+1);
				res[set+'A'] = score[i].A;
				res[set+'B'] = score[i].B;
			}
			return res;
		};
	});
