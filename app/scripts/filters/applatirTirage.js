'use strict';

/**
	* @ngdoc filter
	* @name panelAdminApp.filter:applatirTirage
	* @kind function
	*
	* @description
	* Ce filtre est chargé de transformer le tableau d'objets reçu via l'API en un tableau simple
	*
	* @param {Array} source Le tableau d'objets répertoriant les tirages
*/
angular.module('panelAdminApp')
  .filter('applatirTirage', function () {
		
		return function (entree) {
		  
		  	if(entree){
		  	
			  	var res = [];
			  	
			  	for(var i=0; i<4; i+=1){
			  	
			  		if(entree[i] && entree[i].Team_A && entree[i].Team_B){
			  		
			  			res[i*2] = entree[i].Team_A;
			  			res[i*2+1] = entree[i].Team_B;
			  		}
			  		else{
			  		
			  			res[i*2] = null;
			  			res[i*2+1] = null;
			  		}
			  	
			  	}
			  	
			  	return res;
			}
			
			return null;
		};
  });
