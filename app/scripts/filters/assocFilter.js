'use strict';

/**
	* @ngdoc filter
	* @name panelAdminApp.filter:assocFilter
	* @kind function
	*
	* @description
	* Ce filtre est chargé de transformer les clés d'un objet.
	*
	* @param {Object} source L'objet à modifier
	* @param {Object} assoc L'objet renseignant les associations sous la forme {'ancienneCle':'nouvelleCle'}
	* @param {Boolean} complet Indique si le filtre doit ignorer les champs de l'objet d'origine qui ne sont pas renseignés dans assoc (false) ou bien les conserver (true)
*/
angular.module('panelAdminApp')
	.filter('assocFilter', [function() {
	
		return function(objIn, association, allAssoc) {
		
			if(objIn && association){
			
				var res = {};
				
				// Par défaut on associé toutes les propriétés d'un objet
				allAssoc = !!allAssoc;
			
				if(allAssoc){
			
					var clesAssos=[];
			
					// Liste des clés à changer
					for(var key in association){
				
						clesAssos.push(key);
					}
				
					for(key in objIn){
			
						// Doit changer
						if(clesAssos.indexOf(key) !== -1){
				
							res[association[key]] = objIn[key];
						}
						// Reste semblable à l'objet d'origine
						else{
				
							res[key] = objIn[key];
						}
					}
				}
				else{
			
					angular.forEach(association, function(valeur, cle){
			
						res[valeur] = objIn[cle];
					});
				}
			
				return res;
			}
			
			return null;
		};
	}]);
