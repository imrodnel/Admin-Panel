'use strict';

/**
	* @ngdoc filter
	* @name panelAdminApp.filter:extension
	* @function
	*
	* @param {String} nomFichier Un nom de fichier
	*
	* @description
	* Permet d'extraire l'extension depuis un nom de fichier
 */
angular.module('panelAdminApp')
	.filter('extension', function () {

		return function(nomFichier){
		
			if(nomFichier){
			
				for(var i = nomFichier.length-1; i > 0 && nomFichier.charAt(i) !== '.'; i--){}
			
				// Extension de 3 char.
				if(i === nomFichier.length - 4 && i !== 0){
			
					return nomFichier.substr(i, 4);
				}
			}
			
			return null;
		};
	});
