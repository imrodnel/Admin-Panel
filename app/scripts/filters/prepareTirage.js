'use strict';

/**
	* @ngdoc filter
	* @name panelAdminApp.filter:prepareTirage
	* @kind function
	*
	* @description
	* Ce filtre est chargé de transformer le tableau à envoyer à l'API de façon à ce qu'il correponde à l'entrée attendue
	*
	* @param {Array} source Le tableau à envoyer à l'API
*/
angular.module('panelAdminApp')
  .filter('prepareTirage', function () {
  
    	return function (entree) {
		  
			if(entree){
		  	
			  	var res = {};
			  	
			  	for(var i=0; i<8; i+=1){
			  	
			  		if(entree[i] && entree[i].NameTeam){
			  		
			  			res['team'+(i+1)] = entree[i].NameTeam;
			  		}
			  		else{
			  		
			  			res['team'+(i+1)] = null;
			  		}
			  	
			  	}
			  	
			  	return res;
			}
			
			return null;
		};
  });
