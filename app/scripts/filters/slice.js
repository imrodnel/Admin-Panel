'use strict';

/**
 * @ngdoc filter
 * @name panelAdminApp.filter:slice
 * @function
 * @description
 * Ce filtre est chargé de retourner une copie du tableau qui lui est passé en paramètre en ne conservant que les éléments ayant un indice situé dans la plage définie par les autres paramètres.
 *
 * @param {Array} entree Le tableau d'origine
 * @param {Integer} debut L'indice de début de sélection
 * @param {Integer} fin L'indice de fin de sélection
 */
angular.module('panelAdminApp')
	.filter('slice', function () {
		return function (entree, debut, fin) {
		 	return (entree || []).slice(debut, fin);
		};
	});
