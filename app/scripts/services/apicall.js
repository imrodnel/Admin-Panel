'use strict';

/***
 * @ngdoc service
 * @name panelAdminApp.apicall
 * @description
 * Ce service permet d'effectuer tous les appel à l'API
 */
angular.module('panelAdminApp').
  	provider('apicall', [function(){

	// Variables privées
	var urlApi = 'http://mondomaine.com';
	var urlImages = 'http://mondomaine.com';

	/**
	 * @ngdoc object
	 * @name panelAdminApp.ApiCaller
	 * @description
	 * Objet retourné par le provider apicall
	 */
	function ApiCaller($http, $filter, $q) {
	
		/* Gestion des équipes */
		
		/**
		 * @ngdoc method
		 * @name getEquipes
		 * @methodOf panelAdminApp.apicall
		 * @description
		 * Permet de récupérer les informations sur les équipe du tournois
		 *
		 * @return {Promise} Un promise sur le résultat de la requête
		 */
		this.getEquipes = function() {

			return $http.get(urlApi + '/teams');
		};

		/**
		 * @ngdoc method
		 * @name getEquipe
		 * @methodOf panelAdminApp.apicall
		 * @description
		 * Permet de récupérer les informations sur une équipe du tournois
		 *
		 * @param {Integer} idEquipe L'identifiant de l'équipe dont on souhaite avoir les informations
		 * @return {Object} Un promise sur le résultat de la requête
		 */
		this.getEquipe = function(idEquipe) {

			return $http.get(urlApi + '/teams/' + idEquipe);
		};
		
		/**
		 * @ngdoc method
		 * @name setEquipe
		 * @methodOf panelAdminApp.apicall
		 * @description
		 * Permet de modifier les informations sur une équipe du tournois
		 *
		 * @param {Integer} idEquipe L'identifiant de l'équipe à modifier
		 * @param {Object} dataReq Les nouvelles informations sur l'équipe
		 * @return {Object} Un promise sur le résultat de la requête
		 */
		this.setEquipe = function(idEquipe, dataReq){
			
			return $http.put(urlApi + '/teams/'+idEquipe, dataReq);
		};
		
		/* Gestion des joueurs */
		
		/**
		 * @ngdoc method
		 * @name getJoueursEquipe
		 * @methodOf panelAdminApp.apicall
		 * @description
		 * Permet de récupérer les informations sur les joueurs d''une équipe
		 *
		 * @param {String} isoEquipe Le code iso de l'équipe
		 * @param {String} genre Le sexe des joueurs que l'on souhaite obtenir (voir doc API)
		 * @return {Object} Un promise sur le résultat de la requête
		 */
		this.getJoueursEquipe = function(isoEquipe, genre){

			var urlFinal = urlApi +'/teams/'+isoEquipe+'/players';
			
			if(genre){
			
				urlFinal += '?gender=' + genre;
			}
			
			return $http.get(urlFinal);
		};
	
		/**
		 * @ngdoc method
		 * @name getJoueurs
		 * @methodOf panelAdminApp.apicall
		 * @description
		 * Permet de récupérer les informations sur les joueurs du tournois
		 *
		 * @return {Object} Un promise sur le résultat de la requête
		 */
		this.getJoueurs = function(){

			return $http.get(urlApi + '/players');
		};

		/**
		 * @ngdoc method
		 * @name addJoueur
		 * @methodOf panelAdminApp.apicall
		 * @description
		 * Permet d'ajouter un joueur au tournois
		 *
		 * @param {Object} joueur Les informations sur le joueur à ajouter selon la documentation de l'API
		 * @return {Object} Un promise sur le résultat de la requête
		 */
		this.addJoueur = function(joueur){

			return $http.post(urlApi+'/players', joueur);
		};
    
		/**
		 * @ngdoc method
		 * @name setJoueur
		 * @methodOf panelAdminApp.apicall
		 * @description
		 * Permet de modifier les informations sur un joueur
		 *
		 * @param {Object} joueur Les nouvelles informations du joueur comme spécifié dans la documentation de l'API
		 * @return {Object} Un promise sur le résultat de la requête (+ update photo)
		 */
		this.setJoueur = function(joueur){
			
			return $http.put(urlApi+'/players', joueur);
		};
		
		/**
		 * @ngdoc method
		 * @name updatePhoto
		 * @methodOf panelAdminApp.apicall
		 * @description
		 * Permet de mettre à jour la photo d'un joueur du tournois (seulement compatible IE 9+)
		 *
		 * @param {Integer} idJoueur L'identifiant du joueur concernée
		 * @param {File} infosPhoto Un objet de type file répetoriant les informations pour l'upload
		 * @return {Object} Un promise sur le résultat de la requête
		 */
		this.updatePhoto = function(idJoueur, photo){
			
			var fd = new FormData();
			var deffered = $q.defer();
			var urlPhoto = urlApi + '/players/' + idJoueur + '/photo';
			var extension = $filter('extension')(photo.name);
			var lienFinal = urlImages + '/photos/photo_player_' + idJoueur + extension;
			var paramReq = {
		    
		        transformRequest: angular.identity,
		        headers: {'Content-Type': undefined}
		    };
		    
        	fd.append('photo', photo);

		    $http.put(urlPhoto, fd, paramReq).success(function(){
		    
		    	deffered.resolve(lienFinal);
		    
		    }).error(function(){
		    
		    	$q.reject(deffered);
		    });
		    
		    return deffered.promise;
		};

		/**
		 * @ngdoc method
		 * @name delEquipe
		 * @methodOf panelAdminApp.apicall
		 * @description
		 * Permet de réinitialiser les informations concernant une équipe du tournois
		 *
		 * @param {Integer} indEquipe L'identifiant de l'équipe à réinitialiser
		 * @return {Object} Un promise sur le résultat de la requête
		 */
		this.supprimerEquipe = function(idEquipe){
		
			return $http.delete(urlApi + '/teams/' + idEquipe);
		};

		/**
		 * @ngdoc method
		 * @name delJoueur
		 * @methodOf panelAdminApp.apicall
		 * @description
		 * Permet de supprimer un joueur
		 *
		 * @param {Integer} idJoueur L'identifiant du joueur à supprimer
		 * @return {Object} Un promise sur le résultat de la requête
		 */
		this.supprimerJoueur = function(idJoueur){

			return $http.delete(urlApi + '/players/' + idJoueur);
		};
		
		/* Gestion des matchs */
		
		/**
		 * @ngdoc method
		 * @name getMatchs
		 * @methodOf panelAdminApp.apicall
		 * @description
		 * Permet de récupérer les matchs concernant un tour du tournois
		 *
		 * @param {String} tour Le tour pour lequel on souhaite obtenir les matchs (voir doc API pour les valeurs)
		 *
		 * @return {Object} Un promise sur le résultat de la requête
		 */
		this.getMatchs = function(etapeTournois){
		
			return $http.get(urlApi + '/matches?round=' + etapeTournois);
		};
		
		/**
		 * @ngdoc method
		 * @name getMatch
		 * @methodOf panelAdminApp.apicall
		 * @description
		 * Permet de récupérer les informations concernant un match du tournois
		 *
		 * @param {Integer} idMatch L'identifiant du match pour lequel on doit récupérer les informations
		 *
		 * @return {Object} Un promise sur le résultat de la requête
		 */
		this.getMatch = function(idMatch){
		
			var deffered = $q.defer();
			
			$http.get(urlApi + '/matches/' + idMatch).success(function(donnees){
			
				var association = {
				
					'Category':'category',
					'Team_B':'teamB',
					'Team_A':'teamA',
					'Player_A1':'playerA1',
					'Player_A2':'playerA2',
					'Player_B1':'playerB1',
					'Player_B2':'playerB2',
					'IdPlayerA_1':'playerA1Id',
					'IdPlayerA_2':'playerA2Id',
					'IdPlayerB_1':'playerB1Id',
					'IdPlayerB_2':'playerB2Id',
					'Statut':'status',
					'Court':'court',
					'DateStart':'startDate',
					'DateEnd':'endDate',
					'Winner':'winner'
				};
				
				var res = $filter('assocFilter')(donnees.match, association, true);
				deffered.resolve(res);
				
			}).error(function(){
			
				$q.reject(deffered);
			});
			
			return deffered.promise;
		};
		
		/**
		 * @ngdoc method
		 * @name changeMatchCourt
		 * @methodOf panelAdminApp.apicall
		 * @description
		 * Permet de changer le court associé à un match
		 *
		 * @param {Integer} idMatch	Le match dont on souhaite changer le court
		 * @param {Integer} nouveauCourt Le nouveau court où se déroule le match
		 *
		 * @return {Object} Un promise sur le résultat de la requête
		*/
		this.changeMatchCourt = function(idMatch, nouveauCourt){
		
			var reqData = {
			
				newCourt:nouveauCourt
			};
			
			return $http.put(urlApi + '/matches/' + idMatch +'/court', reqData);
		};
		
		/**
		 * @ngdoc method
		 * @name startMatch
		 * @methodOf panelAdminApp.apicall
		 * @description
		 * Permet de démarrer un match
		 *
		 * @param {Object} infosMatch Les informations sur le match à démarrer
		 *
		 * @return {Object} Un promise sur le résultat de la requête
		*/
		this.startMatch = function(infosMatch){
			
			var reqData = {
			
				action:'START',
				court:infosMatch.court,
				startDate:infosMatch.startDate,
				service:0,
				playerAEq1Id:infosMatch.playerA1Id,
				playerBEq1Id:infosMatch.playerA2Id,
				playerAEq2Id:infosMatch.playerB1Id,
				playerBEq2Id:infosMatch.playerB2Id
			};
			
			return $http.put(urlApi + '/matches/' + infosMatch.id, reqData);
		};
		
		/**
		 * @ngdoc method
		 * @name endMatch
		 * @methodOf panelAdminApp.apicall
		 * @description
		 * Permet de terminer un match en spécifiant un gagnant
		 *
		 * @param {Object} infosMatch Les informations sur le match à terminer
		 *
		 * @return {Object} Un promise sur le résultat de la requête
		*/
		this.endMatch = function(infosMatch){
			
			var reqData = {
			
				action:'END',
				winner:infosMatch.gagnant,
				endDate:infosMatch.endTime
			};
			
			return $http.put(urlApi + '/matches/' + infosMatch.id, reqData);
		};
		
		/**
		 * @ngdoc method
		 * @name matchChangerJoueur
		 * @methodOf panelAdminApp.apicall
		 * @description
		 * Permet de changer le joueur d'un match
		 *
		 * @param {Integer} idMatch			L'identifiant du match à éditer
		 * @param {Integer} idAncienJoueur	L'identifiant de l'ancien joueur
		 * @param {Integer} idNouveauJoueur	L'identifiant du nouveau joueur 
		 *
		 * @return {Object} Un promise sur le résultat de la requête
		*/
		this.matchChangerJoueur = function(idMatch, idAncien, idNouveau){
		
			var reqData = {
			
				oldPlayerId:idAncien,
				newPlayerId:idNouveau
			};
			
			return $http.put(urlApi + '/matches/'+idMatch+'/players', reqData);
		};
		
		/**
		 * @ngdoc method
		 * @name cancelMatch
		 * @methodOf panelAdminApp.apicall
		 * @description
		 * Permet d'annuler un match
		 *
		 * @param {Integer} idMatch	L'identifiant du match qu'on souhaite annuler
		 *
		 * @return {Object} Un promise sur le résultat de la requête
		*/
		this.cancelMatch = function(idMatch){
			
			return $http.put(urlApi + '/matches/' + idMatch, {action:'CANCEL'});
		};
		
		/** Gestion des courts */
		
		/**
		 * @ngdoc method
		 * @name getCourtsDispo
		 * @methodOf panelAdminApp.apicall
		 * @description
		 * Permet de retrouver les courts disponibles pour un match
		 *
		 * @return {Object} Un promise sur le résultat de la requête
		*/
		this.getCourtsDispo = function(){
		
			return $http.get(urlApi + '/courts/available');
		};
		
		/** Gestion des scores */
		
		/**
		 * @ngdoc method
		 * @name getScore
		 * @methodOf panelAdminApp.apicall
		 * @description
		 * Permet de retrouver le score d'un match
		 *
		 * @param {Integer} idMatch Le match dont on souhaite retrouver le score
		 * @return {Object} Un promise sur le résultat de la requête
		*/
		this.getScore = function(idMatch){
		
			return $http.get(urlApi + '/matches/'+idMatch+'/score');
		};
		
		/**
		 * @ngdoc method
		 * @name changeEndMatch
		 * @methodOf panelAdminApp.apicall
		 * @description
		 * Permet de changer la fin d'un match (ainsi que son score)
		 *
		 * @param {Integer} idMatch Le match dont on souhaite retrouver le score
		 * @param {Object} score Le nouveau score du match
		 * @param {String} gagnant Le gagnant du match
		 * @param {String} dateFin La date de fin du match au format AAAA-MM-DD HH:MM:SS
		 * @return {Object} Un promise sur le résultat de la requête
		*/
		this.changeEndMatch = function(idMatch, score, gagnant, dateFin){
		
			var reqData = $filter('applatirScore')(score);
			reqData.winner = gagnant;
			reqData.endDate = dateFin;
			
			return $http.put(urlApi + '/matches/'+idMatch+'/score', reqData);
		};
		
		/* Gestion des superMatchs */
		
		/**
		 * @ngdoc method
		 * @name getSuperMatchs
		 * @methodOf panelAdminApp.apicall
		 * @description
		 * Permet de récupérer les superMatchs concernant un tour du tournois
		 *
		 * @param {String} tour Le tour pour lequel on souhaite obtenir les matchs (voir doc API pour les valeurs)
		 *
		 * @return {Object} Un promise sur le résultat de la requête
		 */
		this.getSuperMatchs = function(etapeTournois){
		
			return $http.get(urlApi + '/supermatches?round=' + etapeTournois);
		};
		
		/* Gestion du tournois */
		
		/**
		 * @ngdoc method
		 * @name getStage
		 * @methodOf panelAdminApp.apicall
		 * @description
		 * Permet de récupérer l'état du tournois
		 *
		 * @return {Object} Un promise sur le résultat de la requête
		 */
		this.getStage = function(){
		
			return $http.get(urlApi + '/tournament/stage');
		};
		
		/* Gestion des tirages */
		
		/**
		 * @ngdoc method
		 * @name getTirage
		 * @methodOf panelAdminApp.apicall
		 * @description
		 * Permet de récupérer les informations concernant l'initialisation des rencontres entre les pays
		 *
		 * @return {Object} Un promise sur le résultat de la requête
		 */
		this.getTirage = function(){
		
			return $http.get(urlApi + '/tournament/tirage');
		};
		
		/**
		 * @ngdoc method
		 * @name getTournoi
		 * @methodOf panelAdminApp.apicall
		 * @description
		 * Permet de récupérer les informations relative au tournoi
		 *
		 * @return {Object} Un promise sur le résultat de la requête
		 */
		this.getTournoi = function(){
		
			return $http.get(urlApi + '/tournament');
		};
		
		/**
		 * @ngdoc method
		 * @name nextStepTournoi
		 * @methodOf panelAdminApp.apicall
		 * @description
		 * Permet de passer à l'étape suivant du tournoi
		 *
		 * @param {String} etape La nouvelle étape pour le tournoi
		 * @return {Object} Un promise sur le résultat de la requête
		 */
		this.nextStepTournoi = function(etape){
		
			var reqData = {
			
				stage : etape
			};
			
			return $http.put(urlApi + '/tournament/stage', reqData);
		};
		
		/**
		 * @ngdoc method
		 * @name setTirage
		 * @methodOf panelAdminApp.apicall
		 * @description
		 * Permet de modifier les informations concernant l'initialisation des rencontres entre les pays
		 *
		 * @param {Object} infosTirage Les informations sur le nouveau tirage
		 * @return {Object} Un promise sur le résultat de la requête
		 */
		this.setTirage = function(infosTirage){
		
			infosTirage = $filter('prepareTirage')(infosTirage);	
			return $http.put(urlApi + '/tournament/tirage', infosTirage);
		};
		
		/**
		 * @ngdoc method
		 * @name authentification
		 * @methodOf panelAdminApp.apicall
		 * @description
		 * 
		 * Permet de récupérer un token auprès de l'API
		 
		 * @param {Object} infosAuth Les informations nécessaires à l'authentification de l'utilisateur
		 * @return {Object} Un promise sur le résultat de la requête
		 */
		this.authentification = function(infosAuth){
		
			return $http.post(urlApi + '/login', infosAuth);
		};
	}

	/* Configuration du provider */
	
	/**
	 * @ngdoc method
	 * @name setUrlApi
	 * @methodOf panelAdminApp.ApiCaller
	 * @description
	 * Permet de spécifier l'URL d'appel à l'API
	 *
	 * @param {String} url L'URL correspondant à l'API (ex : https:x.com:port)
	 */
	this.setUrlApi = function (_url) {
	
		urlApi = _url;
	};
	
	/**
	 * @ngdoc method
	 * @name setUrlImages
	 * @methodOf panelAdminApp.ApiCaller
	 * @description
	 * Permet de spécifier l'URL pour accéder aux imags
	 *
	 * @param {String} url L'URL correspondant au serveur d'image (ex : https:x.com:port)
	 */
	this.setUrlImages = function (_url) {
	
		urlImages = _url;
	};

	// Méthode d'instanciation
	this.$get = function($http, $filter, $q){
	
		return new ApiCaller($http, $filter, $q);
	};
}]);
