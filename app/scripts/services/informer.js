'use strict';

/**
 * @ngdoc service
 * @name panelAdminApp.informer
 * @description
 * # informer
 * Factory in the panelAdminApp.
 */
angular.module('panelAdminApp')
  .factory('informer', ['$timeout', function ($timeout) {

		/**
		 * @ngdoc object
		 * @name panelAdminApp.informer
		 * @description
		 * Objet gérant les notifications provenant de l'application
		 */
		var informer = {};
		
		informer.messages = [];
		
		/**
		 * @ngdoc method
		 * @name informer
		 * @methodOf panelAdminApp.informer
		 * @description
		 * Permet d'ajouter un message au centre de notifications
		 *
		 * @param {String} type Le type du message :
		 *	- success (vert)
		 *	- danger (rouge)
		 *	- warning (orange)
		 * 	- primary (bleu)
		 *
		 * @param {String} msg Le message
		 */
		informer.informer = function(type, msg) {
  
	  		var info = {
				type : 'alert-'+type,
				msg: msg
			};
		
			informer.messages.push(info);
			$timeout(function(){informer.supprimer(info);}, 5000);
	  	};
	  	
	  	/**
		 * @ngdoc method
		 * @name success
		 * @methodOf panelAdminApp.informer
		 * @description
		 * Permet d'ajouter un message vert au centre de notifications
		 *
		 * @param {String} msg Le message
		 */
	  	informer.success = function(msg){
	  	
	  		informer.informer('success', msg);
	  	};
	  	
	  	/**
		 * @ngdoc method
		 * @name info
		 * @methodOf panelAdminApp.informer
		 * @description
		 * Permet d'ajouter un message bleu au centre de notifications
		 *
		 * @param {String} msg Le message
		 */
	  	informer.info = function(msg){
	  	
	  		informer.informer('info', msg);
	  	};
	  	
	  	/**
		 * @ngdoc method
		 * @name error
		 * @methodOf panelAdminApp.informer
		 * @description
		 * Permet d'ajouter un message rouge au centre de notifications
		 *
		 * @param {String} msg Le message
		 */
	  	informer.error = function(msg){
	  	
	  		informer.informer('danger', msg);
	  	};
	  	
	  	/**
		 * @ngdoc method
		 * @name warning
		 * @methodOf panelAdminApp.informer
		 * @description
		 * Permet d'ajouter un message orange au centre de notifications
		 *
		 * @param {String} msg Le message
		 */
	  	informer.warning = function(msg){
	  	
	  		informer.informer('warning', msg);
	  	};
	  	
	  	/**
		 * @ngdoc method
		 * @name getInformations
		 * @methodOf panelAdminApp.informer
		 * @description
		 * Retourne l'ensemble des informations du centre
		 *
		 * @return {Array} L'ensemble des messages
		 */
	  	informer.getInformations = function() {
		
			return informer.messages;
		};
		
		/**
		 * @ngdoc method
		 * @name supprimer
		 * @methodOf panelAdminApp.informer
		 * @description
		 * Permet de supprimer un message du centre de notifications
		 *
		 * @param {Object} msg Le message à supprimer
		 */
		informer.supprimer = function(info) {
  
  			var fileInfos = informer.messages;
			fileInfos.splice(fileInfos.indexOf(info), 1);
	  	};
		
		return informer;
}]);
