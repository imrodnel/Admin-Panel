'use strict';

/**
 * @ngdoc service
 * @name panelAdminApp.intercepteurHttp
 * @description
 * 
 * L'intercepteur HTTP est chargé de rediriger l'utilisateur vers l'authentification s'il ne s'est pas connecté avant d'envoyer une requête de type 'édition' à l'API. Il permet également d'insérer le token d'authentification dans toutes les requêtes vers l'API, de gérer les messages d'erreur provenant de celle-ci et de gérer la reconnexion automatique si nécessaire.
 */
angular.module('panelAdminApp')
	.factory('intercepteurHttp', ['$q', '$injector', '$location', 'infosConnexion', 'informer', function($q, $injector, $location, infosConnexion, informer) {

		return {
		
			'request': function(config){
			
				// Injection du token
				config.headers.ac_token = infosConnexion.token;
				return config;
			},
			'response': function(response){
				
				// Appel à l'API via une methode autre que GET, vérification du code retour
				if(response.data.code){
				
					var code = response.data.code;
					
					// Problème avec le token => Ré-auth (automatique ou non)
					if(code === -98 || code === -99){
					
						// Si on a déjà des identifiants on demande à l'API un nouveau token
						if(infosConnexion.login !== null && infosConnexion.password !== null){
							
							var deffered = $q.defer();
							var apicall = $injector.get('apicall');
						
							apicall.authentification(infosConnexion).success(function(donnees){
							
								infosConnexion.token = donnees.token;
								
								// Rejeu de la requête d'origine
								deffered.resolve($injector.get('$http')(response.config));
								
							}).error(function(){
							
								deffered.reject();
								$location.path('/authentification');
							});
							
							return deffered.promise;
						}
						// On redirige vers la page d'authentification
						else{
							
							informer.info('Redirection vers l\'authentification à cause de l\'expiration du token');
							$location.path('/authentification'); 
							return;
						}
					}
					// Erreur diverses
					else if(code < 0){
					
						var msg = '';
						
						switch(code){
							
							case -1:
							
								msg = 'Erreur dans les paramètres passés à la requête';
								break;
							
							case -2:
							
								msg = 'Erreur inconnue avec la base de données';
								break;
							
							case -3:
							
								msg = 'Erreur avec la base de données';
								break;

							default:
							
								msg = 'Erreur';
								break;
						}
					
						// Récupération du message d'erreur s'il existe
						if(response.data.message){
						
							msg += ' : ' + response.data.message;
						}
					
						informer.error(msg);
						
						return $q.reject(response);
					}
					else{
					
						return response;
					}	
				}
				else{
				
					return response;
				}
			},
			'responseError':function(rejection){
			
				informer.error('Erreur inconnue lors de la requête');
				return $q.reject(rejection);
			}
		};
	}]);
