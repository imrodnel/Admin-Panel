'use strict';

/**
 * @ngdoc service
 * @name panelAdminApp.EquipesManager
 * @description
 * Ce manager est chargé de gérer les entrées des équipes retournées par l'API et gérer les messages d'erreur relatifs à son appel
 */
angular.module('panelAdminApp')
	.service('EquipesManager', ['apicall', 'informer', '$filter', 'chargementGlobal', function(apicall, informer, $filter, chargementGlobal){
		
		var ret = {
		
			entrees : [],

			/**
			 * @ngdoc method
			 * @name rafraichir
			 * @methodOf panelAdminApp.EquipesManager
			 * @description
			 * Permet de rafraichir les entrées du manager via l'API
			*/
			rafraichir : function(){

				chargementGlobal.active = true;
			
				apicall.getEquipes().success(function(donnees){
			
					ret.entrees = donnees.teams;
					console.log(ret.entrees);
					chargementGlobal.active = false;
				
				}).error(function(){
			
					informer.error('Erreur lors de la récupération des équipes');
				});
			},
			
			/**
			 * @ngdoc method
			 * @name supprimerEntree
			 * @methodOf panelAdminApp.EquipesManager
			 * @description
			 * Permet de réinitialiser une équipe
			 * @param {Object} equipe L'équipe à réinitialiser
			*/
			supprimerEntree : function(equipe){
			
				apicall.supprimerEquipe(equipe.IdTeam).success(function(){
				
					informer.success('Équipe réinitialisée avec succès');
				
				}).error(function(){
			
					informer.error('Impossible de réinitialiser l\'équipe');
				});
			},
		
			/**
			 * @ngdoc method
			 * @name editerEntree
			 * @methodOf panelAdminApp.EquipesManager
			 * @description
			 * Permet d'éditer une équipe
			 * @param {Object} equipe L'équipe éditer (identifiant identique à celle à modifier)
			*/
			editerEntree : function(equipe){
		
				var paramReq = {
			
					name : equipe.CompleteName,
					iso : equipe.NameTeam,
					url : equipe.URL
				};
				
				apicall.setEquipe(equipe.IdTeam, paramReq).success(function(){
				
					informer.success('Équipe mise à jour avec succès');
					ret._remplacerEquipeListe(equipe);
				
				}).error(function(){
			
					informer.error('Impossible de modifier l\'équipe');
				});
			},
		
			/* Privé */
		
			_remplacerEquipeListe : function(nouvelleEquipe){
		
				ret.entrees[ret._positionEquipeListe(nouvelleEquipe.IdTeam)] = nouvelleEquipe;
			},
		
			_reinitialiserEquipeListe : function(idEquipe){
		
				apicall.getEquipe(idEquipe).success(function(donnees){
			
					ret._remplacerEquipeListe(donnees.equipe);
			
				}).error(function(){
			
					informer.error('Impossible de récupérer le nouveau statut de l\'équipe');
				});
			},
		
			_positionEquipeListe : function(idEquipe){
		
				return ret.entrees.indexOf($filter('filter')(ret.entrees, {IdTeam:idEquipe})[0]);
			}
		};
		
		return ret;
	}]);
