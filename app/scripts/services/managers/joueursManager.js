'use strict';

/**
 * @ngdoc service
 * @name panelAdminApp.JoueursManager
 * @description
 * Ce manager est chargé de gérer les entrées des joueurs retournées par l'API et gérer les messages d'erreur relatifs à son appel
 */
angular.module('panelAdminApp')
	.service('JoueursManager', ['$filter', 'apicall', 'informer', 'chargementGlobal', function($filter, apicall, informer, chargementGlobal){
  
  		var ret = {
  			
  			entrees : [],
		
			nbEntrees : 0,
			
			/**
			 * @ngdoc method
			 * @name rafraichir
			 * @methodOf panelAdminApp.JoueursManager
			 * @description
			 * Permet de rafraichir les entrées du manager via l'API
			*/
			rafraichir : function(){
		
				chargementGlobal.active = true;
			
				apicall.getJoueurs().success(function(donnees){
			
					for(var i=0; i<donnees.players.length; i++){
					
						donnees.players[i].nationalRankingSingle = donnees.players[i].national_ranking_single;
						donnees.players[i].nationalRankingDouble = donnees.players[i].national_ranking_double;
					}
					
					ret.entrees = donnees.players;
					ret.nbEntrees = donnees.players.length;
					
					chargementGlobal.active = false;
				
				}).error(function(){
			
					informer.error('Erreur lors de la récupération des joueurs');
				});
			},
			
			/**
			 * @ngdoc method
			 * @name ajouterEntree
			 * @methodOf panelAdminApp.JoueursManager
			 * @description
			 * Permet d'ajouter un nouveau joueur au manager
			 * @param {Object} joueur Le nouveau joueur à ajouter
			*/
  			ajouterEntree : function(joueur){
  		
	  			var paramReq = ret._paramJoueur(joueur);
			
	  			apicall.addJoueur(paramReq).success(function(donnees){
				
					informer.success('Joueur ajouté avec succès');
					joueur.IdPlayer = donnees.playerId;
				
					if(joueur.photo){
				
						ret.updatePhoto(joueur);
					}
				
					ret.entrees.push(joueur);
				
				}).error(function(){
			
					informer.error('Impossible d\'ajouter le joueur');
				});
  			},
  		
  			/**
			 * @ngdoc method
			 * @name supprimerEntree
			 * @methodOf panelAdminApp.JoueursManager
			 * @description
			 * Permet de supprimer un joueur du manager
			 * @param {Object} joueur Le joueur à supprimer
			*/
			supprimerEntree : function(joueur){
			
				apicall.supprimerJoueur(joueur.IdPlayer).success(function(){
				
					informer.success('Joueur supprimé avec succès');
					ret._supprimerJoueurListe(joueur.IdPlayer);
				
				}).error(function(){
			
					informer.error('Impossible de supprimer le joueur');
				});
			},
		
			/**
			 * @ngdoc method
			 * @name editerEntree
			 * @methodOf panelAdminApp.JoueursManager
			 * @description
			 * Permet d'éditer un joueur du manager
			 * @param {Object} joueur Le nouveau joueur (identifiant égal à celui qu'il faut éditer)
			*/
			editerEntree : function(joueur){
		
				var paramReq = ret._paramJoueur(joueur);
			
				apicall.setJoueur(paramReq).success(function(){
				
					informer.success('Joueurs mis à jour avec succès');
				
					if(joueur.photo){
				
						ret.updatePhoto(joueur);
					}
				
					ret._remplacerJoueurListe(joueur);
				
				}).error(function(){
			
					informer.error('Impossible de mettre à jour le joueur');
				});
			},

			/* Privé */
			
			updatePhoto : function(joueur){
		
				apicall.updatePhoto(joueur.IdPlayer, joueur.photo).then(function(urlPhoto){
					
					informer.success('Photo du joueur modifiée avec succès');
					joueur.Url = urlPhoto;
					
				}, function(){
			
					informer.error('Impossible de modifier la photo du joueur');
				});
			},
			
			_remplacerJoueurListe : function(nouveauJoueur){
		
				ret.entrees[ret._positionJoueurListe(nouveauJoueur.IdPlayer)] = nouveauJoueur;
			},
		
			_positionJoueurListe : function(idJoueur){
		
				return ret.entrees.indexOf($filter('filter')(ret.entrees, {IdPlayer:idJoueur})[0]);
			},
		
			_supprimerJoueurListe : function(idJoueur){
		
				ret.entrees.splice(ret._positionJoueurListe(idJoueur), 1);
			},
		
			_paramJoueur : function(joueur){
  		
	  			var retour = {
	  			
	  				playerId : joueur.IdPlayer,
	  				team : joueur.Team,
					birthdate : joueur.DateBirth,
					name : joueur.Name,
					firstname : joueur.FirstName,
					sex : joueur.Sex,
					nationalRankingSingle : joueur.nationalRankingSingle,
					nationalRankingDouble : joueur.nationalRankingDouble,
					atp_wta_rankingSingle : null,
					atp_wta_rankingDouble : null
				};
			
				return retour;
  			}
		};
		
		return ret;
	}]);
