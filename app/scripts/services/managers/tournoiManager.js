'use strict';

/**
 * @ngdoc service
 * @name panelAdminApp.TournoiManager
 * @description
 * Ce manager est chargé de gérer les informations concernant le tournoi
 */
angular.module('panelAdminApp')
	.service('TournoiManager', ['apicall', 'informer', function(apicall, informer){
		
		var ret = {
		
			stage : null,

			/**
			 * @ngdoc method
			 * @name rafraichir
			 * @methodOf panelAdminApp.TournoiManager
			 * @description
			 * Permet de rafraichir les informations sur le tournoi
			*/
			rafraichir : function(){

				apicall.getTournoi().success(function(donnees){
			
					ret.stage = donnees.tournament.Stage;
				
				}).error(function(){
			
					informer.error('Erreur lors de la récupération des informations du tournoi');
				});
			},
			
			etapeSuivante : function(){
			
				var relation = {
			
					'SETTING-UP':'1/4',
					'1/4':'1/2',
					'1/2':'Final'
				};
			
				apicall.nextStepTournoi(relation[ret.stage]).success(function(){
				
					informer.success('Le tournoi est bien passé à l\'étape suivante');
					ret.stage = relation[ret.stage];
			
				}).error(function(){
			
					informer.error('Impossible de passer le tournoi à l\'étape suivante');
				});
			}
		};
		
		return ret;
	}]);
