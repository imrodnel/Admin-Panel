'use strict';

/**
 * @ngdoc service
 * @name panelAdminApp.pays
 * @description
 * Constante AngularJS chargée d'associer un nom de pays à son code ISO (2 lettres)
 */
angular.module('panelAdminApp')
  .constant('pays', 

[
	{
		NomPays: 'Afghanistan',
		Iso: 'AF'
	},
	{
		NomPays: 'Albania',
		Iso: 'AL'
	},
	{
		NomPays: 'Algeria',
		Iso: 'DZ'
	},
	{
		NomPays: 'Andorra',
		Iso: 'AD'
	},
	{
		NomPays: 'Angola',
		Iso: 'AO'
	},
	{
		NomPays: 'Antigua and Barbuda',
		Iso: 'AG'
	},
	{
		NomPays: 'Argentina',
		Iso: 'AR'
	},
	{
		NomPays: 'Armenia',
		Iso: 'AM'
	},
	{
		NomPays: 'Australia',
		Iso: 'AU'
	},
	{
		NomPays: 'Austria',
		Iso: 'AT'
	},
	{
		NomPays: 'Azerbaijan',
		Iso: 'AZ'
	},
	{
		NomPays: 'Bahamas',
		Iso: 'BS'
	},
	{
		NomPays: 'Bahrain',
		Iso: 'BH'
	},
	{
		NomPays: 'Bangladesh',
		Iso: 'BD'
	},
	{
		NomPays: 'Barbados',
		Iso: 'BB'
	},
	{
		NomPays: 'Belarus',
		Iso: 'BY'
	},
	{
		NomPays: 'Belgium',
		Iso: 'BE'
	},
	{
		NomPays: 'Belize',
		Iso: 'BZ'
	},
	{
		NomPays: 'Benin',
		Iso: 'BJ'
	},
	{
		NomPays: 'Bhutan',
		Iso: 'BT'
	},
	{
		NomPays: 'Bolivia, Plurinational State of',
		Iso: 'BO'
	},
	{
		NomPays: 'Bosnia and Herzegovina',
		Iso: 'BA'
	},
	{
		NomPays: 'Botswana',
		Iso: 'BW'
	},
	{
		NomPays: 'Brazil',
		Iso: 'BR'
	},
	{
		NomPays: 'Brunei Darussalam',
		Iso: 'BN'
	},
	{
		NomPays: 'Bulgaria',
		Iso: 'BG'
	},
	{
		NomPays: 'Burkina Faso',
		Iso: 'BF'
	},
	{
		NomPays: 'Burundi',
		Iso: 'BI'
	},
	{
		NomPays: 'Cambodia',
		Iso: 'KH'
	},
	{
		NomPays: 'Cameroon',
		Iso: 'CM'
	},
	{
		NomPays: 'Canada',
		Iso: 'CA'
	},
	{
		NomPays: 'Cape Verde',
		Iso: 'CV'
	},
	{
		NomPays: 'Central African Republic',
		Iso: 'CF'
	},
	{
		NomPays: 'Chad',
		Iso: 'TD'
	},
	{
		NomPays: 'Chile',
		Iso: 'CL'
	},
	{
		NomPays: 'China',
		Iso: 'CN'
	},
	{
		NomPays: 'Colombia',
		Iso: 'CO'
	},
	{
		NomPays: 'Comoros',
		Iso: 'KM'
	},
	{
		NomPays: 'Congo',
		Iso: 'CG'
	},
	{
		NomPays: 'Congo, the Democratic Republic of the',
		Iso: 'CD'
	},
	{
		NomPays: 'Costa Rica',
		Iso: 'CR'
	},
	{
		NomPays: 'Côte d\'Ivoire',
		Iso: 'CI'
	},
	{
		NomPays: 'Croatia',
		Iso: 'HR'
	},
	{
		NomPays: 'Cuba',
		Iso: 'CU'
	},
	{
		NomPays: 'Cyprus',
		Iso: 'CY'
	},
	{
		NomPays: 'Czech Republic',
		Iso: 'CZ'
	},
	{
		NomPays: 'Denmark',
		Iso: 'DK'
	},
	{
		NomPays: 'Djibouti',
		Iso: 'DJ'
	},
	{
		NomPays: 'Dominica',
		Iso: 'DM'
	},
	{
		NomPays: 'Dominican Republic',
		Iso: 'DO'
	},
	{
		NomPays: 'Ecuador',
		Iso: 'EC'
	},
	{
		NomPays: 'Egypt',
		Iso: 'EG'
	},
	{
		NomPays: 'El Salvador',
		Iso: 'SV'
	},
	{
		NomPays: 'Equatorial Guinea',
		Iso: 'GQ'
	},
	{
		NomPays: 'Eritrea',
		Iso: 'ER'
	},
	{
		NomPays: 'Estonia',
		Iso: 'EE'
	},
	{
		NomPays: 'Ethiopia',
		Iso: 'ET'
	},
	{
		NomPays: 'Fiji',
		Iso: 'FJ'
	},
	{
		NomPays: 'Finland',
		Iso: 'FI'
	},
	{
		NomPays: 'France',
		Iso: 'FR'
	},
	{
		NomPays: 'Gabon',
		Iso: 'GA'
	},
	{
		NomPays: 'Gambia',
		Iso: 'GM'
	},
	{
		NomPays: 'Georgia',
		Iso: 'GE'
	},
	{
		NomPays: 'Germany',
		Iso: 'DE'
	},
	{
		NomPays: 'Ghana',
		Iso: 'GH'
	},
	{
		NomPays: 'Greece',
		Iso: 'GR'
	},
	{
		NomPays: 'Grenada',
		Iso: 'GD'
	},
	{
		NomPays: 'Guatemala',
		Iso: 'GT'
	},
	{
		NomPays: 'Guinea',
		Iso: 'GN'
	},
	{
		NomPays: 'Guinea-Bissau',
		Iso: 'GW'
	},
	{
		NomPays: 'Guyana',
		Iso: 'GY'
	},
	{
		NomPays: 'Haiti',
		Iso: 'HT'
	},
	{
		NomPays: 'Honduras',
		Iso: 'HN'
	},
	{
		NomPays: 'Hungary',
		Iso: 'HU'
	},
	{
		NomPays: 'Iceland',
		Iso: 'IS'
	},
	{
		NomPays: 'India',
		Iso: 'IN'
	},
	{
		NomPays: 'Indonesia',
		Iso: 'ID'
	},
	{
		NomPays: 'Iran, Islamic Republic of',
		Iso: 'IR'
	},
	{
		NomPays: 'Iraq',
		Iso: 'IQ'
	},
	{
		NomPays: 'Ireland',
		Iso: 'IE'
	},
	{
		NomPays: 'Israel',
		Iso: 'IL'
	},
	{
		NomPays: 'Italy',
		Iso: 'IT'
	},
	{
		NomPays: 'Jamaica',
		Iso: 'JM'
	},
	{
		NomPays: 'Japan',
		Iso: 'JP'
	},
	{
		NomPays: 'Jordan',
		Iso: 'JO'
	},
	{
		NomPays: 'Kazakhstan',
		Iso: 'KZ'
	},
	{
		NomPays: 'Kenya',
		Iso: 'KE'
	},
	{
		NomPays: 'Kiribati',
		Iso: 'KI'
	},
	{
		NomPays: 'Korea, Democratic People\'s Republic of',
		Iso: 'KP'
	},
	{
		NomPays: 'Korea, Republic of',
		Iso: 'KR'
	},
	{
		NomPays: 'Kuwait',
		Iso: 'KW'
	},
	{
		NomPays: 'Kyrgyzstan',
		Iso: 'KG'
	},
	{
		NomPays: 'Lao People\'s Democratic Republic',
		Iso: 'LA'
	},
	{
		NomPays: 'Latvia',
		Iso: 'LV'
	},
	{
		NomPays: 'Lebanon',
		Iso: 'LB'
	},
	{
		NomPays: 'Lesotho',
		Iso: 'LS'
	},
	{
		NomPays: 'Liberia',
		Iso: 'LR'
	},
	{
		NomPays: 'Libya',
		Iso: 'LY'
	},
	{
		NomPays: 'Liechtenstein',
		Iso: 'LI'
	},
	{
		NomPays: 'Lithuania',
		Iso: 'LT'
	},
	{
		NomPays: 'Luxembourg',
		Iso: 'LU'
	},
	{
		NomPays: 'Macedonia, the Former Yugoslav Republic of',
		Iso: 'MK'
	},
	{
		NomPays: 'Madagascar',
		Iso: 'MG'
	},
	{
		NomPays: 'Malawi',
		Iso: 'MW'
	},
	{
		NomPays: 'Malaysia',
		Iso: 'MY'
	},
	{
		NomPays: 'Maldives',
		Iso: 'MV'
	},
	{
		NomPays: 'Mali',
		Iso: 'ML'
	},
	{
		NomPays: 'Malta',
		Iso: 'MT'
	},
	{
		NomPays: 'Marshall Islands',
		Iso: 'MH'
	},
	{
		NomPays: 'Mauritania',
		Iso: 'MR'
	},
	{
		NomPays: 'Mauritius',
		Iso: 'MU'
	},
	{
		NomPays: 'Mexico',
		Iso: 'MX'
	},
	{
		NomPays: 'Micronesia, Federated States of',
		Iso: 'FM'
	},
	{
		NomPays: 'Moldova, Republic of',
		Iso: 'MD'
	},
	{
		NomPays: 'Monaco',
		Iso: 'MC'
	},
	{
		NomPays: 'Mongolia',
		Iso: 'MN'
	},
	{
		NomPays: 'Montenegro',
		Iso: 'ME'
	},
	{
		NomPays: 'Morocco',
		Iso: 'MA'
	},
	{
		NomPays: 'Mozambique',
		Iso: 'MZ'
	},
	{
		NomPays: 'Myanmar',
		Iso: 'MM'
	},
	{
		NomPays: 'Namibia',
		Iso: 'NA'
	},
	{
		NomPays: 'Nauru',
		Iso: 'NR'
	},
	{
		NomPays: 'Nepal',
		Iso: 'NP'
	},
	{
		NomPays: 'Netherlands',
		Iso: 'NL'
	},
	{
		NomPays: 'New Zealand',
		Iso: 'NZ'
	},
	{
		NomPays: 'Nicaragua',
		Iso: 'NI'
	},
	{
		NomPays: 'Niger',
		Iso: 'NE'
	},
	{
		NomPays: 'Nigeria',
		Iso: 'NG'
	},
	{
		NomPays: 'Norway',
		Iso: 'NO'
	},
	{
		NomPays: 'Oman',
		Iso: 'OM'
	},
	{
		NomPays: 'Pakistan',
		Iso: 'PK'
	},
	{
		NomPays: 'Palau',
		Iso: 'PW'
	},
	{
		NomPays: 'Panama',
		Iso: 'PA'
	},
	{
		NomPays: 'Papua New Guinea',
		Iso: 'PG'
	},
	{
		NomPays: 'Paraguay',
		Iso: 'PY'
	},
	{
		NomPays: 'Peru',
		Iso: 'PE'
	},
	{
		NomPays: 'Philippines',
		Iso: 'PH'
	},
	{
		NomPays: 'Poland',
		Iso: 'PL'
	},
	{
		NomPays: 'Portugal',
		Iso: 'PT'
	},
	{
		NomPays: 'Qatar',
		Iso: 'QA'
	},
	{
		NomPays: 'Romania',
		Iso: 'RO'
	},
	{
		NomPays: 'Russian Federation',
		Iso: 'RU'
	},
	{
		NomPays: 'Rwanda',
		Iso: 'RW'
	},
	{
		NomPays: 'Saint Kitts and Nevis',
		Iso: 'KN'
	},
	{
		NomPays: 'Saint Lucia',
		Iso: 'LC'
	},
	{
		NomPays: 'Saint Vincent and the Grenadines',
		Iso: 'VC'
	},
	{
		NomPays: 'Samoa',
		Iso: 'WS'
	},
	{
		NomPays: 'San Marino',
		Iso: 'SM'
	},
	{
		NomPays: 'Sao Tome and Principe',
		Iso: 'ST'
	},
	{
		NomPays: 'Saudi Arabia',
		Iso: 'SA'
	},
	{
		NomPays: 'Senegal',
		Iso: 'SN'
	},
	{
		NomPays: 'Serbia',
		Iso: 'RS'
	},
	{
		NomPays: 'Seychelles',
		Iso: 'SC'
	},
	{
		NomPays: 'Sierra Leone',
		Iso: 'SL'
	},
	{
		NomPays: 'Singapore',
		Iso: 'SG'
	},
	{
		NomPays: 'Slovakia',
		Iso: 'SK'
	},
	{
		NomPays: 'Slovenia',
		Iso: 'SI'
	},
	{
		NomPays: 'Solomon Islands',
		Iso: 'SB'
	},
	{
		NomPays: 'Somalia',
		Iso: 'SO'
	},
	{
		NomPays: 'South Africa',
		Iso: 'ZA'
	},
	{
		NomPays: 'Spain',
		Iso: 'ES'
	},
	{
		NomPays: 'Sri Lanka',
		Iso: 'LK'
	},
	{
		NomPays: 'Sudan',
		Iso: 'SD'
	},
	{
		NomPays: 'SuriNomPays',
		Iso: 'SR'
	},
	{
		NomPays: 'Swaziland',
		Iso: 'SZ'
	},
	{
		NomPays: 'Sweden',
		Iso: 'SE'
	},
	{
		NomPays: 'Switzerland',
		Iso: 'CH'
	},
	{
		NomPays: 'Syrian Arab Republic',
		Iso: 'SY'
	},
	{
		NomPays: 'Taiwan, Province of China',
		Iso: 'TW'
	},
	{
		NomPays: 'Tajikistan',
		Iso: 'TJ'
	},
	{
		NomPays: 'Tanzania, United Republic of',
		Iso: 'TZ'
	},
	{
		NomPays: 'Thailand',
		Iso: 'TH'
	},
	{
		NomPays: 'Timor-Leste',
		Iso: 'TL'
	},
	{
		NomPays: 'Togo',
		Iso: 'TG'
	},
	{
		NomPays: 'Tonga',
		Iso: 'TO'
	},
	{
		NomPays: 'Trinidad and Tobago',
		Iso: 'TT'
	},
	{
		NomPays: 'Tunisia',
		Iso: 'TN'
	},
	{
		NomPays: 'Turkey',
		Iso: 'TR'
	},
	{
		NomPays: 'Turkmenistan',
		Iso: 'TM'
	},
	{
		NomPays: 'Tuvalu',
		Iso: 'TV'
	},
	{
		NomPays: 'Uganda',
		Iso: 'UG'
	},
	{
		NomPays: 'Ukraine',
		Iso: 'UA'
	},
	{
		NomPays: 'United Arab Emirates',
		Iso: 'AE'
	},
	{
		NomPays: 'United Kingdom',
		Iso: 'GB'
	},
	{
		NomPays: 'United States',
		Iso: 'US'
	},
	{
		NomPays: 'Uruguay',
		Iso: 'UY'
	},
	{
		NomPays: 'Uzbekistan',
		Iso: 'UZ'
	},
	{
		NomPays: 'Vanuatu',
		Iso: 'VU'
	},
	{
		NomPays: 'Venezuela, Bolivarian Republic of',
		Iso: 'VE'
	},
	{
		NomPays: 'Viet Nam',
		Iso: 'VN'
	},
	{
		NomPays: 'Western Sahara',
		Iso: 'EH'
	},
	{
		NomPays: 'Yemen',
		Iso: 'YE'
	},
	{
		NomPays: 'Zambia',
		Iso: 'ZM'
	},
	{
		NomPays: 'Zimbabwe',
		Iso: 'ZW'
	}
]
  );
