'use strict';

/**
 * @ngdoc service
 * @name panelAdminApp.RESSOURCES
 * @description
 * Répertorie les variables d'accès aux ressources de l'application
 */
angular.module('panelAdminApp')
	.constant('RESSOURCES', (function(){
	
		var domaine = 'http://5.196.21.161';
		
		return {
			DOMAINE	: domaine,
			API		: domaine + ':2222',
			IMAGES	: domaine,
			DRAPEAUX: domaine + '/drapeaux/'
		};
	})());
