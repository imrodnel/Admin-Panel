'use strict';

/**
 * @ngdoc service
 * @name panelAdminApp.chargementGlobal
 * @description
 * Valeur AngularJS permettant de spécifier si un chargement est actif (true) ou non (false)
 */
angular.module('panelAdminApp')
  .value('chargementGlobal', {active:false});
