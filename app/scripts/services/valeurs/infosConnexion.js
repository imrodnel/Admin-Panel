'use strict';

/**
 * @ngdoc service
 * @name panelAdminApp.infosConnexion
 * @description
 * Valeur AngularJS contenant les informations de connexion d'un utilisateur (login, password, token)
 */
angular.module('panelAdminApp')
	.value('infosConnexion',
		/* Pour debug
		{
			login:'perceval',
			password:'faux',
			token:'x' // Espace pour soumettre un token interprété par le serveur
		}*/
		// Pour déploiement
		{
			login:null,
			password:null,
			token:'x' // Espace pour soumettre un token interprété par le serveur
		}//
	);
