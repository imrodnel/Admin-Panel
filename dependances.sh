#!/bin/bash

npm install karma --save-dev
npm install socket.io --save-dev
npm install di --save-dev
npm install log4js --save-dev
npm install grunt-karma --save-dev
npm install grunt-contrib-imagemin --save-dev
npm install grunt-svgmin --save-dev
npm install grunt-ngdocs --save-dev
