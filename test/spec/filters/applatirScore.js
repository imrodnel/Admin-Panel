'use strict';

describe('Filter: applatirScore', function () {

	// load the filter's module
	beforeEach(module('panelAdminApp'));

	// initialize a new instance of the filter before each test
	var applatirScore;
	beforeEach(inject(function ($filter) {
		applatirScore = $filter('applatirScore');
	}));

	it('Doit applatir un score de manière correcte', function () {

		var scoreEntree = [
		
			{
				A:1,
				B:2
			},
			{
				A:3,
				B:4
			},
			{
				A:5,
				B:6
			},
			{
				A:7,
				B:8
			},
			{
				A:9,
				B:10
			}
		];
		
		var sortieAttendue = {
		
			set1A:1,
			set1B:2,
			set2A:3,
			set2B:4,
			set3A:5,
			set3B:6,
			set4A:7,
			set4B:8,
			set5A:9,
			set5B:10
		};
		
		expect(applatirScore(scoreEntree)).toBe(sortieAttendue);
	});

});
