'use strict';

describe('Filter: applatirTirage', function () {

	// load the filter's module
	beforeEach(module('panelAdminApp'));

	// initialize a new instance of the filter before each test
	var applatirTirage;
	beforeEach(inject(function ($filter) {
		applatirTirage = $filter('applatirTirage');
	}));

	it('Doit applatir un tirage provenant de l\'API', function () {
	
		var entree = [
		
			{
				TeamA:'HE',
				TeamB:'JK'
			},
			{
				TeamA:'AB',
				TeamB:'CD'
			},
			{
				TeamA:'KL',
				TeamB:'MN'
			},
			{
				TeamA:'PO',
				TeamB:'TS'
			},
		];
		
		var sortie = ['HE', 'JK', 'AB', 'CD', 'KL', 'MN', 'PO', 'TS'];
		
		expect(applatirTirage(text)).toBe(sortie);
	});

	it('Doit retourner une erreur en cas de paramètre incorrect', function () {
		
		expect(applatirTirage(null)).toBeNull();
	});
});
