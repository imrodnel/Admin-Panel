'use strict';

describe('Filter: assocFilter', function () {

	// load the filter's module
	beforeEach(module('panelAdminApp'));

	// initialize a new instance of the filter before each test
	var assocFilter;
	beforeEach(inject(function ($filter) {
		assocFilter = $filter('assocFilter');
	}));

	it('Doit retourner une association sans les champs non-définis', function () {
	
		var entree = {
		
			test:'ok',
			cle:'ko',
			noTraf:'noTrad'
		};
		
		var sortie = {
		
			testTrad:'ok',
			cleTrad:'ko'
		};
		
		var assoc = {
		
			'test':'testTrad',
			'cle':'cleTrad'
		}
		expect(assocFilter(entree, assoc, false)).toBe(sortie);
	});
	
	it('Doit retourner une association avec les champs non-définis', function () {
	
		var entree = {
		
			test:'ok',
			cle:'ko',
			noTraf:'noTrad'
		};
		
		var sortie = {
		
			testTrad:'ok',
			cleTrad:'ko'
		};
		
		var assoc = {
		
			'test':'testTrad',
			'cle':'cleTrad'
		}
		expect(assocFilter(entree, assoc, true)).toBe(sortie);
	});
	
	it('Doit retourner null en cas d\'erreur dans le paramètre entrée', function () {
	
		expect(assocFilter(null, {}, null)).toBeNull();
	});
	
	it('Doit retourner null en cas d\'erreur dans le paramètre association', function () {
	
		expect(assocFilter({}, null, null)).toBeNull();
	});
});
