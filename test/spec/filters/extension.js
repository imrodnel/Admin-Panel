'use strict';

describe('Filter: extension', function () {

	// load the filter's module
	beforeEach(module('panelAdminApp'));

	// initialize a new instance of the filter before each test
	var filtre;
	beforeEach(inject(function ($filter){
	
		filtre = $filter('extension');
	}));

	it('Doit retourner une extension à partir d\'un nom de fichier correct', function(){

		expect(filtre('test.jpg')).toBe('.jpg');
	});
	
	it('Doit retourner null à partir d\'un nom de fichier incorrect', function(){

		expect(filtre('testjpg')).toBeNull();
	});
	
	it('Doit retourner null à partir d\'un nom de fichier avec extension incorrecte', function(){

		expect(filtre('testj.pg')).toBeNull();
	});
	
	it('Doit retourner null à partir d\'un nom de fichier ne contenant qu\'une extension', function(){

		expect(filtre('.jpg')).toBeNull();
	});
	
	it('Doit retourner null en cas de paramètre erroné', function(){

		expect(filtre(null)).toBeNull();
	});

});
