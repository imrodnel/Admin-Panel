'use strict';

describe('Filter: prepareTirage', function () {

	// load the filter's module
	beforeEach(module('panelAdminApp'));

	// initialize a new instance of the filter before each test
	var prepareTirage;
	beforeEach(inject(function ($filter){

	prepareTirage = $filter('prepareTirage');
	}));

	it('Doit retourner un tirage adéquat pour l\'API', function () {
    
    	var entree = {
		
			{
				NameTeam:'BF'
			},
			{
				NameTeam:'TU'
			},
			{
				NameTeam:'BJ'
			},
			{
				NameTeam:'KL'
			},
			{
				NameTeam:'OF'
			}
		};
		
		var sortie = {
		
			team1:'BF',
			team2:'TU',
			team3:'BJ',
			team4:'KL',
			team5:'OF'
		};
		
		expect(prepareTirage(entree)).toEqual(sortie);
	});
	
	it('Doit retourner une erreur en cas de paramètre incorrect', function () {
		
		expect(prepareTirage(null)).toBeNull();
	});
});
